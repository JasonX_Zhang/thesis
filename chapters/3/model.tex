\startchapter{HESS Topologies Comparison and Modeling Strategy}
\label{chp-model}

Parts of the work presented in this chapter was presented at the 2011 ASME International Mechanical Engineering Congress and Exposition:

Citation\cite{self1}: \bibentry{self1}

In this chapter, the superiorities of a hybrid energy storage system are discussed first. Then three standard HESS topologies are compared by using the typical pulsed current load. Based on the comparison, the semi-active topology with an ideal DC-DC converter in the ultracapacitor end is selected as the HESS structure in this research work. Finally, a semi-active HESS model is built and simulated by using Matlab and Python. This model is then used to simulate the proposed intelligent energy allocation algorithm in Chapter \ref{qlearning}.

\section{Hybrid Energy Storage System}
In order to improve energy storage system (ESS), the possibility of the integration of two (or more) energy sources should be researched, with the objective of utilizing the best characteristics of each, producing a hybrid energy storage system (HESS). Nowadays, hybridization of high-energy batteries with ultracapacitors is a common choice, since, from the analysis above, they have complementary characteristics that make them attractive for a HESS. Combining batteries and ultracapacitors can create an energy storage system with both high peak power and high energy density. Besides, a HESS has following superiorities

\begin{itemize}
  \item Higher Energy Efficiency

 "Delivering high power for a short period of time is deadly to batteries, but it is the ultracapacitor strongest suit \cite{schindall}." The relationship of the discharge time and discharge current in a battery can be modeled by Peukert capacity.\cite{larminie_lowry_2003}

\begin{equation}
  C_p = I^k\cdot T
\end{equation}

In which $C_p$ is the Peukert capacity, $I$ is discharge current, $T$ is discharge time, and $k$ is the Peukert coefficient, which is usually 1.1-1.3 for lead acid, and 1.05-1.2 for nickel metal hydride and lithium ion \cite{emadi_2005}. Battery delivers less charge (the integral of current) when discharged faster. Reference \cite{jossen_2006} shows that pulsed discharge profile results in increased cell temperature, considering the same average current. Because of the system losses, the efficiency for battery is lower when the frequency of the current is higher.

Since the ultracapacitor is able to deliver or receive energy in peak power situations, if a battery is hybridized with an ultracapacitor, its demand would become closer to the average power demand. Therefore, the system losses are reduced and the efficiency is improved.

  \item Longer Battery Life

 As the battery cost is a significant part in the price of the whole car, the life of batteries is very important to customer acceptance of EVs. However, high charge or discharge rates shorten the battery life, even including high current-rate lithium-ion batteries \cite{buchmann,buchmann_2004}. Reference \cite{choi_lim_2002} analyses the life reduction of cobalt-based lithium-ion cells for high charge or discharge current. As to a HESS, the batteries’ better working condition created by ultracapacitor makes the battery life longer. Here additional remarks should be made that ultracapacitors have a very long life, significantly higher than batteries.

  \item Better Thermal Management

 Ultracapacitors can operate under a wider temperature range than batteries\cite{Miller_2008}. When used together, ultracapacitors can attenuate the reduction in the power available from batteries in extreme temperature conditions. Furthermore, if the weather is so cold that batteries fail to work, ultracapacitors can supply energy to the thermal management system in a pure electric vehicle to preheat the batteries.

  \item Lower System Cost

As discussed previously, the rapid-developing ultracapacitor technology allows achieving power density of several thousand W/kg at a relatively low cost. Some Li-ion polymer batteries reach the same power density, but at much higher prices. Therefore, HESS is a better choice than pure batteries to achieve high power density.

\end{itemize}

\section{Three topologies of battery ultracapacitor hybrids}

\subsection{Typical pulsed current load}
The majority of electric and hybrid vehicles possess certain load profile characteristics, described by relatively high peak-to-average power requirements. Such loads can be closely represented by a typical pulsed current load \cite{Kuperman}, which is illustrated in Figure \ref{c3_1}.

%############################### Figure 3.1 #############################
\begin{figure}[hbt]
  \center
  \includegraphics[width=100mm, scale=0.5]{chapters/3/images/c3_f1.png}
  \caption{Pulsed Current Load}
  \label{c3_1}
\end{figure}
%#########################################################################

The consumption profile is characterized by a periodic rectangular pulse train, alternating between two current levels, $i_{L,MIN}$ and $i_{L,MAX}$ with period $T$ and duty cycle $D$,

\begin{equation}
  i_L(t) = i_{L,MIN}u(t)+ \sum_{k=0}^{N}(i_{L,MAX}-i_{L,MIN})[u(t-kT)-u(t-DT-kT)]
\end{equation}

where $u(t)$ is a unit step function and $N$ is the number of operation periods. Also, $i(t)$ can be written as:

\begin{equation}
  i_L(t) = i_{L,AVE}(t) + i_{L,DYN}(t)
\end{equation}

where, $i_{L,AVE}(t)$ is the average current and $i_{L,DYN}(t)$ is the dynamic part.

\begin{equation}
  i_{L,AVE}(t)= \frac{1}{T}\int_{0}^{T}i_L(t)dt = D\cdot i_{L,MAX}+(1-D)\cdot i_{L,MIN} = I_{L,AVE}
\end{equation}

Furthermore, the load current can also be represented by Fourier series as:

\begin{equation}
\label{tpc}
  i_{L}(t) = I_{L,AVE} + \sum_{n=1}^{\inf}I_{L,n}\cdot cos(n\frac{2\pi}{T}t + \phi_{n}(jn\frac{2\pi}{T}))
\end{equation}

where the current harmonics magnitude is

\begin{equation}
  i_{L,n} = \pi D (i_{L,MAX}-i_{L,MIN})[sin(n\pi D)]
\end{equation}

and $\phi$ is the current harmonics phase.


\subsection{Passive hybrid}

Till now, the most common battery-ultracapacitor hybrid topology is the passive hybrid, which has been studied by many researchers \cite{pagano_piegari_2007,cericola_2010,penella_gasulla_2010,liu_wang_cheng_maly_2009} and employed in commercial products \cite{menachem_yamin_2004, choi_kim_yoon_2004, hu_deng_suo_pan_2009}. In a passive topology, as shown in Figure \ref{c3_2}, the batteries and ultracapacitors are connected in parallel with each other and the load. The advantages of passive topology are the simplicity and the absence of power electronics and control circuitries, which reduces the cost and increasing reliability. However, the main disadvantage is that the load current is distributed in a nearly uncontrolled manner between the battery and the ultracapacitor.

%############################### Figure 3.2 #############################
\begin{figure}[hbt]
  \center
  \includegraphics[width=100mm, scale=0.5]{chapters/3/images/c3_f2.png}
  \caption{Passive HESS}
  \label{c3_2}
\end{figure}
%#########################################################################

The ultracapacitor is represented by the nominal capacitance $C$ and the internal resistance $r_C$. $r_B$ is the internal resistance of the battery. The load current is Typical Pulsed Current (TPC), which is represented by Equation \ref{tpc}.

From \cite{kuperman_aharon_2011}, we obtain the system power loss is:

\begin{equation}
  P_{LOSS} = P_{LOSS,BAT} + P_{LOSS,C} = r_BI_{L,AVE}^{2} + \frac{1}{2}\sum_{n}I_{L,n}^{2}\cdot r_{P,n}
\end{equation}

where,

\begin{equation}
  r_{P,n} = r_B\mid H_C(jn\frac{2\pi}{T})\mid^2 + r_C(1-\mid H_C(jn\frac{2\pi}{T})\mid)^2
  \label{rpn}
\end{equation}

and,

\begin{equation}
  H_C(j\omega) = \frac{1+j\omega Cr_C}{1+j\omega C(r_B+r_C)} = \mid H_C(j\omega)\mid e^{j\theta C^{(j\omega)}} = \sqrt{\frac{1+(\omega Cr_C)^2}{1+(\omega C(r_B+r_C)^2}}e^{j\theta C^{(j\omega)}}
\end{equation}

If only batteries drive the load without ultracapacitors, the system losses are:

\begin{equation}
  P_{LOSS} = P_{LOSS,BAT} = r_BI_{BAT,RMS}^{2} = (I_{L,AVE}^{2} + \frac{1}{2}\sum_{n}I_{L,n}^{2})\cdot r_B
\end{equation}

Since both $\mid H_C(j\omega)\mid$ and $1-\mid H_C(j\omega)\mid$ are less than unity as well as $r_C \ll r_B$, it can be obtained from Equation \ref{rpn} that $r_{P,n} < r_B$. Therefore, the system losses are reduced as a result of hybridization.
According to \cite{gao_ehsani_2006, pagano_piegari_2007, cericola_2010, penella_gasulla_2010}, battery current in time domain is:

\begin{equation}
\label{ibt}
\begin{aligned}
i_B(t) = &i_{L,MIN} + (i_{L,MAX} - i_{L,MIN})\times \sum_{k}([1-\frac{r_B}{r_B+r_C}e^{-\omega _B(t-kT)}]u(t-kT)\\
&-[1-\frac{r_B}{r_B+r_C}e^{-\omega _B(t-DT-kT)}]u(t-DT-kT))
\end{aligned}
\end{equation}
where $\omega_B = \frac{1}{(r_B+r_C)C}$


From Equation \ref{ibt}, conclusion can be drawn that during the high load demand, both the battery and the ultracapacitor supply charge to the load. During the low load demand, the battery supplies both the load and the capacitor. Furthermore, battery current ripple reduces, and battery terminal voltage dips become lower than in the battery-only case. Hence, the battery is more efficiently utilized. Increasing the capacitance will force the maximum and minimum values of the battery currents to become closer to each other. The discharge curve of a passive hybrid converges towards the discharge at $I_{L,AVE}$ curve as the capacitance is increased. As a result, either more energy can be drawn from the same battery or a battery with lower rating can be utilized. However, a negative byproduct of capacitance increase by connecting capacitors in parallel is weight/volume/price increase. On the other hand, the internal resistance of the capacitor pack is decreased, and as a result the losses are decreased. If one of the negative consequences of capacitance increase cannot be tolerated, semi-active or fully active hybrid should be considered. In addition, there is a trade-off between the allowed load voltage ripple and capacitor utilization \cite{kuperman_aharon_2011}.

\subsection{Semi-active hybrid}
If a DC–DC converter is employed to the battery and ultracapacitor banks, a semi-active hybrid is composed \cite{cao_emadi_2009, allegre_bouscayrol_trigui_2009, liu_zhang_zhu_2009,jmiller_2008}. The semi-active topology enhances the performance of the passive hybrid at the price of an additional DC–DC converter and control circuitry. Three different ways of creating a semi-active hybrid are considered, battery semi-active, capacitor semi-active and load semi-active
\begin{itemize}
  \item Battery Semi-active Hybrid

In this topology, the DC-DC converter is connected between the battery and the load, as illustrated in Figure \ref{semib} \cite{voorden_elizondo_paap_verboomen_sluis_2007,govindaraj_lukic_emadi_2009,khaligh_li_2010}. The output current of the DC-DC converter is controlled to follow the current $I_{L,AVE}$. The main advantage of such a topology is the ability to control the battery current at a near constant value despite the load current variations. This allows significant battery performance improving in lifetime, energy efficiency and operating temperature. In addition, voltage matching between the battery and the load is no longer required.

\begin{figure}[hbt]
  \center
  \includegraphics[width=100mm, scale=0.5]{chapters/3/images/semib.png}
  \caption{Battery semi-active hybrid topology}
  \label{semib}
\end{figure}


The batter voltage and current are

\begin{equation}
  v_{BAT} = \frac{v_L}{K_{BAT}(t)} ,\quad i_B = K_{BAT}(t)\cdot \frac{I_{L,AVE}}{\eta_{DCDC,BAT}}
\end{equation}

where $K_{BAT}(t)$ and $\eta_{DCDC,BAT}$ are the battery converter voltage conversion ratio and efficiency, the system losses are

\begin{equation}
  P_{LOSS} = P_{LOSS,BAT} + P_{LOSS,C} = r_B(K_{BAT}(t)\cdot \frac{I_{L,AVE}}{\eta_{DCDC,BAT}})^2 + \frac{1}{2}r_C\sum_{n}I_{L,n}^{2}
\end{equation}

The main disadvantage of the topology is the variations of the load voltage during capacitor charging/discharging.

  \item Capacitor Semi-active Hybrid

In the capacitor semi-active topology, a DC-DC converter is located between the capacitor and the load, as shown in Figure \ref{semic} \cite{cheng_wismer_2007, camara_dakyo_gualous_nichita_2009, shah_karndhar_maheshwari_kundu_desai_2009, rui_hongwen_xiaowei_yi_2010, miller_deshpande_dougherty_bohn_2009}. Such a topology allows controlling of the capacitor current.

\begin{figure}[hbt]
  \center
  \includegraphics[width=100mm, scale=0.5]{chapters/3/images/semic.png}
  \caption{Capacitor semi-active hybrid topology}
  \label{semic}
\end{figure}

As a result of decoupling between the ultracapacitor and the battery, the utilization of the ultracapacitor energy is improved. If the DC-��DC converter output current is controlled to follow the dynamic part of the load current $i_{L,DYN}(t)$, current of the battery and the ultracapacitor are:

\begin{equation}
  i_B = i_L - i_{L,DYN} = i_{L.AVE} ,\quad i_C = K_{UC}(t)\cdot \frac{i_{L,DYN}}{\eta_{DCDC,UC}}
\end{equation}

where $K_{UC}(t)$ and $\eta_{DCDC,UC}$ are the ultracapacitor converter voltage conversion ratio and efficiency, respectively. Therefore, the system losses are formulated as

\begin{equation}
  P_{LOSS} = P_{LOSS,BAT} + P_{LOSS,C} = r_B(I_{L,AVE}^{2} + \frac{1}{2}r_C\sum_{n}(K_{UC}(t)\cdot \frac{I_{L,n}}{\eta_{DCDC,UC}})^2
\end{equation}

Note that the in the capacitor semi-active configuration, the load voltage possesses no ripple (since a nearly constant current is drawn from the battery) but is unregulated, decreasing as the battery is depleted according to the battery discharge curve at $I_{L,AVE}$.

  \item Load Semi-active Hybrid

As to a load semi-active configuration, a DC-DC converter is placed between the load and electrical sources (parallel branch of battery/ ultracapacitor), as shown in Figure \ref{semil}. \cite{yuanbin_qingnian_changjian_boshi_2009, amjadi_williamson_2010, glavin_chan_armstrong_hurley_2008, onar_khaligh_2008}
This topology is developed from the passive hybrid topology. It allows a mismatch between the battery voltage (and hence the ultracapacitor voltage rating) and the load. According to Figure 6


\begin{figure}[hbt]
  \center
  \includegraphics[width=100mm, scale=0.5]{chapters/3/images/semil.png}
  \caption{Load semi-active hybrid topology}
  \label{semil}
\end{figure}

\begin{equation}
  v_L = K_L(t)\cdot v_{BAT} ,\quad i_S = K_L(t)\cdot \frac{i_L}{\eta_{DCDC,L}}
\end{equation}

Where, $i_S$ is the current supplied by the battery ultracapacitor parallel branch to the DC-DC converter inputs. Substituting (16) in to (5),

\begin{equation}
  i_S(t) = K_L(t)\cdot \frac{i_{L,AVE}}{\eta_{DCDC,L}} + \sum_{n}K_L(t)\cdot \frac{i_{L,n}}{\eta_{DCDC}}cos(n\frac{2\pi}{T}t + \phi_n(jn\frac{2\pi}{T}))
\end{equation}

Furthermore, the current of battery and ultracapacitor is:

\begin{equation}
  I_{B,n}(t) = K_L(t)\cdot \frac{I_{L,n}}{\eta_{DCDC,L}}\cdot \mid H_C(jn\frac{2\pi}{T})\mid, \quad I_{C,n}(t) = K_L(t)\cdot \frac{I_{L,n}}{\eta_{DCDC,L}}\cdot \mid 1-H_C(jn\frac{2\pi}{T})\mid
\end{equation}

And the system losses are

\begin{equation}
  P_{LOSS} = P_{LOSS,BAT} + P_{LOSS,C} = (\frac{K_L(t)}{\eta_{DCDC,L}})^2 \times \lbrace r_BI_{L,AVE}^{2} + \frac{1}{2}\sum_{n}I_{L,n}^{2}\cdot r_{P,n}
\rbrace
\end{equation}

However, it does not change the fact that the battery supplies part of the dynamic current and the ultracapacitor available charge is still limited.
\end{itemize}

\subsection{Active hybrid}
As to an active hybrid, two DC–DC converters are employed \cite{miller_deshpande_dougherty_bohn_2009}. There are three topologies: battery series active, capacitor series active and parallel active. The fully active hybrid brings the system to an ultimate performance; however, as a result, the control complexity is increased.
\begin{itemize}
  \item Battery Series Active Hybrid
\end{itemize}
This topology is an enhancement of the battery semi-active hybrid, as illustrated in Figure \ref{actbl}. It solves the disadvantages of ultracapacitor voltage variations and matching by placing an additional DC-DC converter between the ultracapacitor and the load. However, it comes at the price of an extra full rating DC-DC converter and the reduced efficiency, since there are two conversion stages between the battery and the load.

\begin{figure}[hbt]
  \center
  \includegraphics[width=100mm, scale=0.5]{chapters/3/images/actbl.png}
  \caption{Battery series active hybrid}
  \label{actbl}
\end{figure}


The system losses are:
\begin{equation}
\begin{aligned}
  P_{LOSS} = P_{LOSS,BAT} + P_{LOSS,C} = r_B(K_{BAT}(t)\cdot K_L(t)\cdot \frac{I_{L,AVE}}{\eta_{DCDC,BAT} \cdot \eta_{DCDC,L}})^2  \\+ \frac{1}{2}r_C\sum_{n}(K_L(t)\cdot \frac{I_{L,n}}{\eta_{DCDC,L}})^2
\end{aligned}
\end{equation}
\begin{itemize}
  \item Capacitor Series Active Hybrid
\end{itemize}
This topology is an enhancement of the capacitor semi-active hybrid, as shown in Figure \ref{actcl}. It solves the disadvantages of battery voltage reduction and matching by placing an additional DC-DC converter between the battery and the load. However, it again comes at the price of an extra full rating DC–DC converter and the reduced efficiency, since there are two conversion stages between the ultracapacitor and the load.

\begin{figure}[hbt]
  \center
  \includegraphics[width=100mm, scale=0.5]{chapters/3/images/actcl.png}
  \caption{Ultracapacitor series active hybrid topology}
  \label{actcl}
\end{figure}

The losses of the topology are given by
\begin{equation}
\begin{aligned}
  P_{LOSS} = P_{LOSS,BAT} + P_{LOSS,C} = r_B(K_L(t)\cdot \frac{I_{L,AVE}}{\eta_{DCDC,L}})^2
  \\+ \frac{1}{2}r_C\sum_{n}(K_{UC}(t)\cdot K_L(t)\cdot \frac{I_{L,n}}{\eta_{DCDC,UC} \cdot \eta_{DCDC,L}})^2
\end{aligned}
\end{equation}

\begin{itemize}
  \item Parallel Active Hybrid
\end{itemize}
This topology is the optimal active hybrid. It solves the disadvantages of ultracapacitor voltage variations, achieves a nearly constant current flow from the battery and voltage mismatch between the battery and the load by placing two DC-DC converters. The topology, which combines the advantages of battery and ultracapacitor semi-hybrids, is shown in Figure \ref{actbc}.

\begin{figure}[hbt]
  \center
  \includegraphics[width=100mm, scale=0.5]{chapters/3/images/actbc.png}
  \caption{Parallel active hybrid topology}
  \label{actbc}
\end{figure}


The system losses are
\begin{equation}
  P_{LOSS} = P_{LOSS,BAT} + P_{LOSS,C} = r_B(K_{BAT}(t)\cdot \frac{I_{L,AVE}}{\eta_{DCDC,BAT}})^2
  + \frac{1}{2}r_C\sum_{n}(K_{UC}(t) \frac{I_{L,n}}{\eta_{DCDC,UC}})^2
\end{equation}
The losses are the lowest among the three active hybrid topologies.

\subsection{Conclusions for topologies of battery-ultracapacitor hybrids}
The passive hybrid is the most simple and cheap topology, however, the utilization of battery and ultracapacitor is relatively lower and the load regulation is not good enough. On the other hand, the fully active hybrid attains the best performance, however, the cost is high and the control strategy is complicated. The semi-active hybrid might be a good trade-off between the performance and the circuit complexity and price.

\section{Matlab-Python joint simulation framework}
As we know, there exist a lot of useful tools in Python, such as the TensorFlow by Google, facilitating the machine learning research. On the other hand, Matlab is super powerful in simulating vehicle and power electronics models. In order to enjoy the superiorities of both Python and Matlab, we propose a novel Matlab(Autonomie)-Python joint simulation framework in this work. The vehicle model is created in Autonomie and simulated in Matlab. The HESS model and the proposed IEA algorithm is developed in Python. Details about this joint framework can be found in Figure \ref{joint-framework}. The Matlab model and Python model are connected by vehicle electrical load data in $json$ format. In fact, these models are not executed simultaneously. The vehicle model in Matlab is firstly run, and the current load data is generated and saved into a $json$ file. Then python loads those data from the $json$ file and executes the HESS model.

\begin{figure}
  \center
  \includegraphics[width=0.7\textwidth, height = 0.45\textwidth]{chapters/3/images/model_framework.eps}
  \caption{Matlab(Autonomie)-Python joint simulation framework}
  \label{joint-framework}
\end{figure}

In order to build the HESS model in Python, a simulation package, called Simulator is developed in Python. More details about this package and the simulation platform hosting can be found in the Appendix \ref{simulatorapp}.

\section{HESS modeling}
This work is financially supported by Canadian Natural Resources and Applied Sciences Endowment Fund. The vehicle model was exported into Matlab/Simulink from Autonomie, which is a simulation software provided by the Argonne National Laboratory. In Autonomie, 2004 Prius is the only vehicle model built and validated from the real car by the Autonomie group.\cite{Kim2012} Since Toyota does not open all parameters of its cars for modeling, 2004 Prius is our only option.

The battery in the HESS is exactly the same battery in the Prius, while the ultracapacitor is the Maxwell HTM Power Series 390v BOOSTCAP Ultracapacitor Modules. More detailed parameters about these two devices can be found in the following sections.

\subsection{Battery model}
As introduced in the previous section, the model in Autonomie is a data-driven model. Detailed battery parameters can be found in Table. \ref{bpara}.

\begin{table}[htb]
\caption{Pruis battery parameters}
\begin{center}
\label{bpara}
\begin{tabular}{|c|c|c|c|}
\hline
Parallel module & $1$ & Series module & $28$ \\
\hline
Cells in each module (series) & $6$ & Cell nominal voltage & $1.2V$ \\
\hline
Cell max/min voltage & $0.95V$/$1.55V$ & Cell mass & $0.17kg$\\
\hline
Module mass & $1.02kg$ & Pack mass & $28.56kg$\\
\hline
Packaging mass & $7.14kg$ & Module heat capacity & $521J/K$ \\
\hline
Max charging current & $-170.8864A$ & Max discharging current & $184.5218A$\\
\hline
Capacity & $6.5Ah$ & Pack Nornimal Voltage & $201.6V$\\
\hline
Power Density & $1.3kW/kg$ & Energy Density & $46Wh/kg$\\
\hline
\end{tabular}
\end{center}
\end{table}

Note that, the battery internal resistor is not constant, it changes with the battery SOC. In Figure\ref{rchg}, relation between the resistor and SOC can be found. Moreover, Toyota provides a 8-year/100,000 miles warranty for this battery and the replacement cost of this one is $\$3,939$. \cite{John2016}

\begin{figure}[ht]
  \center
  \includegraphics[width=0.7\textwidth, height = 0.45\textwidth]{chapters/3/images/re_soc.eps}
  \caption{Internal Resisotr - SOC relation}
  \label{rchg}
\end{figure}

In this work, we are trying to rebuild the Prius battery model into a mathematically model in Python. According to \cite{buller2002}, the Rint battery model is shown in Figure \ref{rint}

\begin{figure}[ht]
  \center
  \includegraphics[width=0.55\textwidth, height = 0.4\textwidth]{chapters/3/images/rint.png}
  \caption{Battery Model}
  \label{rint}
\end{figure}

Note that the voltage source $U_{oc}$ is a controlled-source, which is controlled by the battery SOC. The relation between the SOC and the cell voltage exported from Autonomie is plotted. Given a current load, the system output can be calculated as follows:

\begin{equation}
	U_L = U_{oc}(SOC) - I_LR_o
\end{equation}

\begin{figure}[ht]
  \center
  \includegraphics[width=0.7\textwidth, height = 0.45\textwidth]{chapters/3/images/battery_soc.eps}
  \caption{OCV - SOC relation}
  \label{ocvsoc}
\end{figure}

where $U_{oc}(SOC)$ is a function of battery SOC and battery SOC can be calculated by using Equation \ref{cal_soc}. Moreover, we also add the battery thermal performance simulation in the resistor $R_o$.  For each module in the battery pack, the temperature changing can be estimated by Equation \ref{btempcal}.
\begin{equation}
	\Delta Temp = \frac{I_L^2R_oN_{cell}}{C_{mod} \cdot m_{mod}}
	\label{btempcal}
\end{equation}
where, $N_{cell}$ is the cell number in a module, $C_{mod}$ is the module heat capacity and $m_{mod}$ is the module mass. The battery model is then compared with the model in Autonomie. The results can be checked in the following plots.


\begin{figure}[htb]
\subfloat[Current source]{\includegraphics[height=0.3\textwidth,width=0.49\linewidth]{chapters/3/images/bcom_source.eps}}
\subfloat[Battery SOC]{\includegraphics[height=0.3\textwidth,width=0.49\linewidth]{chapters/3/images/bcom_soc.eps}}\\
\subfloat[Battery temperature]{\includegraphics[height=0.3\textwidth,width=0.49\linewidth]{chapters/3/images/bcom_temp.eps}}
\subfloat[Battery voltage]{\includegraphics[height=0.3\textwidth,width=0.49\linewidth]{chapters/3/images/bcom_vol.eps}}\\
\caption{Simulation results compare: battery model}
\end{figure}

\subsection{Ultracapacitor model}

The ultracapacitor used in this work is the Maxwell HTM Power Series 390v BOOSTCAP Ultracapacitor Module. Its detailed parameters can be found in Table \ref{cpara}. The cost of this Module is $\$3332$.

\begin{table}[htb]
\caption{Maxwell ultracapacitor parameters}
\begin{center}
\label{cpara}
\small
\setlength\tabcolsep{2pt}
\begin{tabular}{|c|c|c|c|}
\hline
Nornimal Voltage & $390V$ & Maximal Voltage & $394V$ \\
\hline
Surge Voltage & $406V$ & Nominal Capacitance & $17.8F$ \\
\hline
Tolerance Capacitance & $+20\%/-0\%$ & Resistance & $65m\Omega$\\
\hline
Energy & $282Whkg$ & Maximal Continuous/Pulse Current& $150A/950A$\\
\hline
\end{tabular}
\end{center}
\end{table}

The one-order ultracapacitor model (See Figure \ref{cap_model}) is built in Python. Then experiment is conducted to test the model. In the experiment, the ultracapacitor is charged from $0$ SOC by a current load. The results are compared with the model in Matlab and plotted as follows.

\begin{figure}[ht]
  \center
  \includegraphics[width=0.55\textwidth, height = 0.4\textwidth]{chapters/3/images/cap_model.png}
  \caption{One order ultracapacitor model}
  \label{cap_model}
\end{figure}

\begin{figure}[htb]
\subfloat[Current source]{\includegraphics[height=0.3\textwidth,width=0.49\linewidth]{chapters/3/images/ccom_source.eps}}
\subfloat[Voltage]{\includegraphics[height=0.3\textwidth,width=0.49\linewidth]{chapters/3/images/ccom_vol.eps}}\\
\caption{Simulation results compare: ultracapacitor model}
\end{figure}

Note that, since we are using the one-order ultracapacitor model and in order to avoid solving ordinary differential equations (ODEs), we convert the ODE into discrete integral equations. For example, for a passive HESS shown in Figure \ref{c3_2}, when $v_B(t) > v_C(t)$, we have:
\begin{equation}
v_B(t) - [(i_L(t) + i_C(t)]\cdot r_B = i_C(t)\cdot r_C + v_C(t)
\end{equation}
And
\begin{equation}
i_C(t) = \frac{d[v_C(t)]}{dt}
\end{equation}
So we have
\begin{equation}
C \cdot \frac{d[v_C(t)]}{dt} = \frac{v_B(t) - v_C(t)-i_L(t)\cdot r_B}{r_B+r_C}
\end{equation}
We convert this ODE to a discrete integral equation and use Trapezoidal Rule to solve it. Then we have:
\begin{equation}
i_C(k) = \frac{v_B(k) - v_C(k-1) - \frac{T}{2C}\cdot i_C(k-1) - i_L(k)\cdot r_B}{r_B + r_C + \frac{T}{2C}}, \quad \textrm{when} \quad k>0
\end{equation}
