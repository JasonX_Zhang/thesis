\startfirstchapter{Introduction}
\label{chapter:introduction}

% **************************** Section 1.1 ***************************
\section{Background}

Today, increasing environmental concerns, dwindling petroleum reserves, and increasing gas price demand alternative energy solutions for the dominating energy consuming transportation sector, especially ground vehicles. Meanwhile the total number of vehicles globally will increase from 700 million to 2.5 billion over the next 50 years \cite{chan_wong_2004}. Further improvement of fuel economy and finding alternative or renewable energy sources for vehicle propulsion has been the focus of vehicle technology development worldwide.

There are a bunch of advanced technologies that help to reduce petroleum consumption and tailpipe emissions. One straightforward approach is to enhance powertrain efficiency and lower vehicle resistance forces. This technical path can be further divided into four major technical categories: engine, transmission, vehicle techniques and hybrid techniques. A comprehensive survey of those techniques can be found in \cite{epa}. Hybridization of powertrain is widely considered as a practical and effective solution to remarkably improve ICE efficiency and emissions in near future \cite{eia}. Hybrid vehicle (HV) is defined as a vehicle with two or more energy storage system (ESS), both of which must provide propulsion power–either together or independently \cite{sae_report}. Specifically, in addition to conventional fuel tank, the secondary ESS could be flywheel, compressed air tank, battery, ultracapacitor as well as combination of battery-ultracapacitor, as summarized in right-bottom block of Figure \ref{c1_1} \cite{john_miller_2004} \cite{x_wang_2008} \cite{e_tanaka_2013}. These types of HVs differ from each other greatly from operation principle, performance and FE benefits as well as costs. Those HVs equipped with battery as ESS are in a monopoly position from aspects of both count and type, in comparison with other competitors. The second strategy of reducing petroleum consumption is to shift use of petroleum to other energy sources. Various alternative energy sources and corresponding powertrains are summarized at two sides of Figure \ref{c1_1}, according to energy sources, on-board energy and propulsion systems. Primary solutions include flexible-fuel vehicle (FFV) and electrified vehicles (EV).

% ============== figure 1.1 ==============
\begin{figure}[hbt]
  \center
  \includegraphics{chapters/1/images/1.png}
  \caption{Alternative fuel and powertrain solutions}
  \label{c1_1}
\end{figure}
% ========================================

% --------------- Section 1.1.1 -----------------
\subsection{Electrified Vehicles}

Although flexible-fuel vehicle (FFV) will continue expanding market penetration, electrified vehicles (EV) will be the most practical and influential choice in the following decades for a couple of reasons: a) electric energy is pivotal element for diversification of energy sources, beneficial for energy security; b) petroleum will continue to be primary fuel of on-land vehicles in decades, so hybridization of vehicle will play a critical role in improving mass-production vehicle efficiency and reducing emissions; c) hybrid electric vehicles shown at left-bottom corner of Figure \ref{c1_1} is intersection of electrification and hybridization approaches, providing a wide range of technical solutions \cite{eia, epa}. Electrified vehicle, especially hybrid vehicle, combines hybridization and electrification, possessing special potential.

Powertrain architecture, which refers to layout and energy flow paths among powertrain components, is an important index of xEV powertrain. Many scholars have categorized xEV into Series Hybrid, Parallel Hybrid and Power-split Hybrid, with emphasizes on EM, power electronics or modeling, respectively \cite{john_miller_2004} \cite{a_emadi_2005} \cite{m_ehsani_2007} \cite{c_c_chan_2010} \cite{m_ehsani_2005}. Meanwhile, electrification level, which has great impact on architecture design and selection, breaks up electrified vehicle into five categories of hybrid vehicle (micro Hybrid EV, mild Hybrid EV and strong Hybrid EV, Plug-in Hybrid Electric Vehicle (PHEV), Extended Range Electric Vehicle (ER-EV)), Pure Electric Vehicle (PEV) and Fuel Cell Electric Vehicle (FCEV).

The first appearance of EVs dates back to the early 1830. These EVs were not commercial vehicles as they used non-rechargeable batteries. It will take an additional half a century before batteries are developed sufficiently to be used in commercial vehicles \cite{larminie}. In 1989 Ferdinand Porsche, an employee of the Austrian company Jacob Lohner \& Co, developed a drive system based on fitting an electric motor to each front wheel, without using a transmission \cite{web_4}. During the $20$th century petroleum powered vehicles showed absolute dominance over the EVs. The reasons are easily understood when the specific energy of petroleum fuel is compared to that of batteries. For example, the specific energy of diesel, i.e. energy stored per kilogram, is about $12600$ $Wh/kg$, while the highest reported specific energy of Lithium-air batteries is about $360$ $Wh/kg$ \cite{kraytsberg, zhang}. Moreover, the diesel is much cheaper with $0.15$ $e/kWh$, compared to the optimistic price of about $180$ $e/kWh$ for energy optimized batteries projected by the United States Advanced Battery Consortium \cite{linda}.

Nevertheless, the electrification of vehicles has increased again in the 21st century motivated by the air pollution, global warming and rapid depletion of the Earth’s petroleum resources. Obviously, in order to develop efficient and cost effective EVs, one of the key technical challenges and bottle necks needed to be improved or solved in vehicle electrification is the high performance, reliable, long-lasting and low-cost on board electrical energy storage system (ESS).

% --------------- Section 1.1.2 -----------------
\subsection{Electrical energy storage system}

 As mentioned above, an electrified vehicle could be categorized into Series Hybrid EV, Parallel Hybrid EV or Power-split Hybrid EV depending upon how the power from its ICE and electric motors/generators (M/Gs) are blended. Also, an electrified vehicle can be classified into micro Hybrid EV, mild Hybrid EV and strong Hybrid EV, Plug-in Hybrid EV, Extended Range EV, Pure EV or even Fuel Cell EV based on its electrification level. Obviously, no matter in what category an EV is, it always needs a reliable and efficient electrical energy storage system to store the electrical energy.

The most common energy storage device used in electrified vehicles is the battery. Batteries have been the technology of choice for most electrified vehicle applications, because they can store large amounts of energy in a relatively small volume and weight and provide suitable levels of power for many applications. Shelf and cycle life have been a problem/concern with most types of batteries, but people have learned to tolerate this shortcoming due to the lack of an alternative. In recent times, due to the increasing electrification level, power requirements in a number of electrified vehicle applications have increased markedly and have exceeded the capability of batteries of standard design.\cite{a_burke2014} This has led to the design of special high power, pulse batteries often with the sacrifice of energy density and cycle life. Ultracapacitor have been developed as an alternative to pulse batteries. As an attractive alternative, ultracapacitors enjoy much longer shelf and cycle life than batteries. By 'much' is meant about one order of magnitude higher.\cite{a_burke2014}

In order to improve the efficiency, performance, cost and life of on-board electrical energy storage system, appropriate integration of two or more energy sources has been researched to allow the best characteristics of each type to be fully utilized, leading to a hybrid energy storage system (HESS). As discussed above that battery alone cannot serve as the optimal energy source for electrified vehicles due to their power/energy trade-offs, as shown in the Ragone plot of Figure \ref{c1_2} \cite{electropaedia}. Combining batteries and ultracapacitors can create an ESS with both high peak power and high energy density.

% ============== figure 1.2 ==============
\begin{figure}[hbt]
  \center
  \includegraphics{chapters/1/images/2.png}
  \caption{Ragone plot}
  \label{c1_2}
\end{figure}
% ========================================

% **************************** Section 1.1 ***************************
\section{Research contributions}
As mentioned above, a hybrid energy storage system (HESS) is able to provide Electrified Vehicles with both high peak power and high energy density. However, this new hybrid structure also raises a question on how to control battery and ultracapacitor together properly or even optimally.

This work mainly focuses on developing an intelligent control algorithm which coordinates the battery and ultracapacitor inside a HESS by optimizing their the energy flow. This new proposed algorithm is able to adapt itself to new driving trips. Together with this intelligent control algorithm, this research also contributes with a new joint simulation method, two new driving pattern recognition algorithms focusing on automatic feature selection and better dynamic property separately. These two new pattern recognition algorithms are used by the proposed intelligent control algorithm, since it allocates the energy between battery and ultracapacitor according to the current driving trip estimated by the recognized driving patterns. The proposed new joint simulation method is used to simulate the new intelligent control algorithm and validate its performance. Detailed contributions are listed as follows:

\begin{enumerate}
  \item Both battery and ultracapacitor have complicated electrochemical and electrothermal processes. In order to carry out Reinforcement Learning (RL) from the HESS data and simulate/validate the proposed algorithm, a Matlab-Python combined simulation method is proposed in this work. By using this method, we are able to generate training data set for training the value network inside the proposed Intelligent Energy Allocation algorithm. Also, by using this method, we are able to validate this work.

  \item The system efficiency of three major topologies of battery-ultracapacitor HESS are calculated and compared by using a typical pulsed current load in this manuscript. This work was published on the Proceedings of the ASME 2011 International Mechanical Engineering Congress \& Exposition, IMECE2011 \cite{self1}. Based on this comparison analysis work, the semi-active topology with an ideal DC-DC converter in the ultracapacitor side is selected as the HESS structure in this research work

  \item For the selected HESS structure, we proposed an intelligent energy allocation algorithm (IEA) by employing a reinforcement learning. This proposed algorithm is able to adapt itself to different daily driving modes. It keeps learning to generate optimal policies to prolong the battery life and improve the HESS efficiency from the data collected during the daily driving. The more the car is driven, the higher the HESS efficiency would be. The fact, that the proposed IEA algorithm is based on the driving modes recognized from the daily driving, leads to the following two contributions.

  \item A more efficient and robust driving pattern recognition technique, extended Support Vector Machine (SVM) with embedded feature selection ability, is proposed in this work. Besides statistical significance, this proposed SVM also takes into account the accessibility and reliability of features during feature selection, so as to enable the driving condition discrimination system to achieve higher recognition efficiency and robustness. This work was published in the Journal of Franklin Institute \cite{self2}. This work helps to obtain a higher  accuracy of the driving pattern recognition, hence the better performance of the IEA algorithm.

  \item A novel classification algorithm, Inertial Matching Pursuit Classification (IMPC), is also proposed in this work. Compared with the traditional methods using SVM or Neural Networks, IMPC can recognize the diving patterns by using vehicle velocity data sampled in less sampling time, so that the accuracy of estimating the overall driving conditions for entire driving trip is improved. This work has been submitted to the IEEE Transaction on Intelligent Transportation Systems. This work enables the IEA to recognize the driving pattern within a shorter time and therefore helps the IEA to have a better dynamic performance.

 \end{enumerate}

\section{Outline of the dissertation}
This dissertation is organized in seven chapters. After introducing the research background and reviewing some related topics in Chapter 1 and Chapter 2, we firstly compared the three different topologies for the battery-ultracapacitor hybrid storage system in Chapter 3. Also in that chapter, a combined simulation method is proposed by employing both Matlab and Python. At the end of that chapter, we will have the vehicle model and the simulation platform ready for the following chapters. Chapter 4 and Chapter 5 are both focusing on the pattern recognition problem. In Chapter 4, we are focusing on the feature selection problem, while in Chapter 5, we are focusing on improving the dynamic property of the pattern recognition system. Feature selection approaches can be divided into three categories: filters, wrappers and embedded approaches. Among them, embedded approaches can simultaneously determine features and classifier during the training process and hence enjoys the highest efficiency. \cite{Neumann2005} In spite of the lack of oracle property, 1-norm SVM, as a very standard embedded approach, has both good performance in feature selection and classification. \cite{zou2006}. In Chapter 4, the extended Support Vector Machine (SVM) is developed from the standard 1-norm SVM. This proposed method is able select less features with better observabilities. In Chapter 5, the Inertial Matching Pursuit Classification (IMPC), is proposed especially for recognizing vehicle driving patterns in a shorter time period. Earlier research \cite{Yang2008} has shown that if the length of the sampling time of the vehicle speed reaches or exceeds $3$ minutes, the characteristic of the current driving pattern can be effectively recognized by common pattern recognition algorithms, such as Support Vector Machine (SVM) and Neural Networks. However, a Driving Pattern Recognition system with $3$ minutes sampling time has really bad resolution in daily driving. Therefore, the Inertial Matching Pursuit Classification is developed from the Compressed Sensing algorithm because of its ability of extracting high-level abstract information (features) from sparse data. After Chapter 4 and Chapter 5, the accurately recognized driving pattern is fed to the Intelligent Energy Allocation algorithm, which is finally proposed in Chapter 6.
This IEA algorithm needs current driving pattern as input, so that the Trip Mode can be accurately recognized in time. After each driving pattern is recognized, this algorithm generates the optimal control actions to separate the required current between battery and ultracapacitor according to the learned Q network. In Chapter 7, we conclude the entire thesis and discuss the future work. More details about the structure can be found in the following list and Figure \ref{chapter_diag}

% ============== figure 1.3 ==============
\begin{figure}[hbt]
  \center
  \includegraphics[width=0.9\textwidth, height = 0.55\textwidth]{chapters/1/images/chapter_flow.eps}
  \caption{Outline of the dissertation}
  \label{chapter_diag}
\end{figure}
% ========================================

\begin{description}
\item[\textbf{Chapter 1}] Introduction and claims of this research
\item[\textbf{Chapter 2}] Review on the HESS design/control problems and related topics
\item[\textbf{Chapter 3}] Compare the HESS topologies and build the object models and simulation platform. Propose the combined simulation method. Parts of this work was presented at the 2011 ASME International Mechanical Engineering Congress and Exposition as a conference paper
\item[\textbf{Chapter 4}] Present the new extended SVM for driving pattern recognition and discuss about the feature selection problem of the driving pattern recognition system. An early version of the work was published on the Journal of the Franklin Institute in 2015.
\item[\textbf{Chapter 5}] Present the Inertial Matching Pursuit Classification algorithm for driving pattern recognition and discuss about the sampling period problem. This work was submitted to the IEEE transactions on intelligent transportation systems as a journal paper.
\item[\textbf{Chapter 6}] Develop the intelligent energy allocation algorithm (IEA)
\item[\textbf{Chapter 7}] Summarize the research work and discuss the future work.
\end{description}
