---
title: Responses to comments and Log of changes
author: Jason X Zhang
---

Received on 2018-03-12 | 37 comments in all

### -> Comment 1: Page 2
>Zuomin and I both co-supervisors

Sorry, I forgot to change. This is from the template. Fixed

### -> Comment 2: Page 4
>extract

Fixed

### -> Comment 3: Page 6
>a

Fixed

### -> Comment 4: Page 18
>C

Currently, only the first letter in the section title is capitalized. Do I need to capitalize the first letter of every word in the section title?

### -> Comment 5: Page 18
>s

Fixed

### -> Comment 6: Page 18
>Add another paragraph to start, that relates the contirbutions (i.e. in last section identified that an HESS is useful); now connect the contributions together, and clarify the key point that they all work together to enable dev of an HESS control strategy.

Added

### -> Comment 7: Page 18
>this first sentance is too generic; is this some random methods? or an integrated set of methods? I think the latter

Fixed

### -> Comment 8: Page 18
>make specific to vehicles as opposed to other applications

Added

### -> Comment 9: Page 19
>so is the model unique, or just the implementation?

The model is just the implementation. But the joint simulation method is unique and novel.

### -> Comment 10: Page 19
>delete

Deleted

### -> Comment 11: Page 20
>T watch your capitalizations

Fixed

### -> Comment 12: Page 21
>this section needs works; it's still not clear how everything relates.
I'd suggest a flow diagram showing how the algorithms fits together to solve the overall HESS control problem.

Added more explanation and a diagram.

### -> Comment 13: Page 36
>THis is good! lays out what the chapter is for



### -> Comment 14: Page 36
>reference

I guess I don't understand this clearly. Do you mean I need to add a cross reference here for the last chapter? Added

### -> Comment 15: Page 47
>Up to here it's good for a PhD thesis; gives us governing equations, etc. Need to refine this section to what's novel, and just state teh fact there was a tool developed to enable the research. NOt all the details of the software (that should go in an appendix)

Refined this section. Removed some general introduction about Matlab and Python.

### -> Comment 16: Page 49
>these code-specific things should go in an Appendix

Fixed

### -> Comment 17: Page 50
>THese next two should be a sepsarte sub-section, as they are example system parameters thta get used later

Fixed

### -> Comment 18: Page 56
>:

Fixed

### -> Comment 19: Page 56
>reference chapter #'s explicitly

Fixed

### -> Comment 20: Page 56
>State what final model/variables comes out that is useful for HESS control

Added

### -> Comment 21: Page 56
>Delete

Deleted

### -> Comment 22: Page 56
>if this were true, then Chpt 6 should be moved before Chpt 4

Deleted

### -> Comment 23: Page 75
>.

Added


### -> Comment 24: Page 75
>Relate back to the previous chapter and expand on the basic approach. Also state what comes out of this chpt in terms of  a model/variables useful for the final HESS control

Added

### -> Comment 25: Page 75
>Delete

Deleted

### -> Comment 26: Page 99
>what baout chpt 3 usefulness?

Added

### -> Comment 27: Page 99
>5

Fixed


### -> Comment 28: Page 112
>DOD

Fixed

### -> Comment 29: Page 112
>so clear then what's the point of this model if you can't use it?

Added explanation. This section is to prove two things. Firstly, battery DOD, temperature and current are the key factors of battery SOH. That's why in IEA, we use data aggregated from these signals as the system states. Secondly, we are draw a conclusion that the energy allocation system of the HESS does not need to be updated in a high frequency. This conclusion is very important. Because it provides us with the feasibility of applying IEA algorithm on the HESS control problem. The IEA algorithm is based on driving pattern recognition. As discussed in Chapter \ref{svm} and Chapter \ref{impc}, the driving pattern recognition system usually needs a relatively long time period to finish recognition (averagely around hundred seconds). If the energy allocation policy needs to be updated in a high frequency, then we might not able to recognize current driving pattern correctly, hence the failure of the IEA algorithm.

### -> Comment 30: Page 112
>Delete

Deleted

### -> Comment 31: Page 118
>Delete

Deleted

### -> Comment 32: Page 119
>So by hear, I'm lost as to how all these different algorithsm relate? Am I having to somehow choose between them?
Need another section that explains how everything fits together

In practical scenario, we will always have the uncertain Trip Mode cases. However, if the current driving Trip Mode can be easily recognized in the early stage of the trip, then this control problem can be converted to a given Trip Mode problem. Added this explanation in the end of the "IEA for an uncertain Trip Mode" subsection.

### -> Comment 33: Page 130
>can't see in 3D....

Changed dots to bars

### -> Comment 34: Page 132
>I see results in these figures, but I have no way of knowing (no metrics) or comparison to standard battery non-HESS. Need more work here to define how these new algorithsm are better. Less battery degratation, efficiencies better, etc

The sum of the reward function is used as the metric to evaluate and compare the performances of different algorithm. The reward function value of each driving pattern is also plotted in the eighth figure in the results figure sets. However, I didn't clearly explain this.

Added more explanation about how the reward function is designed. Added a new subsection discussing about the performance metric. Added another table in the end of this chapter to compare different methods.

### -> Comment 35: Page 134
>A diagram to link everything together into a practical HESS control methods would be nice

Added two diagrams for both IEA for an uncertain Trip Mode and IEA for a given Trip Mode

### -> Comment 36: Page 134
>Again, what metrics do you have to say its better than standard battery non-HESS, or even other ways of operating an HESS (e.g. just in high power conditions)

Added table to compare the performance. Note that battery non-HESS is the situation when action $a=1$ in the fixed-cutoff-frequency method.

### -> Comment 37: Page 136
>what about real-world testing? costs/mass/complexity of an HESS going forward relative to new battery developments etc

Added real-world testing. Don't fully understand the "costs/mass/complexity of an HESS going forward relative to new battery developments etc"
