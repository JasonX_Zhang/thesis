\startchapter{Powertrain Feature Selection Based on Extended Support Vector Machine}
\label{svm}

An early version of the work presented in this chapter was presented on the Journal of the Franklin Institute in 2015:

Citation\cite{self2}: \bibentry{self2}

As mentioned in Chapter 1, the proposed IEA algorithm needs the recognized driving patterns as input to optimize the energy flow between battery and ultrocapacitor in HESS. Therefore, this chapter and Chapter \ref{impc} will focus on the topics related to the driving pattern recognition. This chapter focuses on selecting significant features for driving pattern recognition, while next chapter focuses on recognizing driving pattern in a much shorter time period.

The extended Support Vector Machine (SVM) with embedded feature selection ability is proposed in this chapter. Besides statistical significance, this proposed SVM also takes into account the accessibility and reliability of features during feature selection. This enables the driving pattern recognition system to achieve higher recognition efficiency and robustness, hence the better performance of the IEA algorithm. Note that the algorithm proposed in this chapter is not related to the one in Chapter 5. These two algorithms are independent to each other and dealing with different concerns in driving pattern recognition.

\section{Introduction}

To effectively recognize and distinguish different driving conditions during vehicle operation, representation of different driving conditions by using several key parameters (features) is essential. In HEV applications, a number of state variables of the vehicle powertrain can be regarded as features for driving condition recognition. In order to choose statistically significant features for recognition, research into feature extraction and selection for the pattern recognition in vehicle applications is necessary. Higher recognition efficiency, less time in feature data collection and higher accuracy in classification, can be achieved by using a smaller number of features with higher significance in driving condition recognition. A lot of work has been done in this area. Ericsson has successfully reduced 62 different features to 16 independent driving condition features using Factorial Analysis  \cite{Ericsson2001}. In \cite{Xi2011}, four significant features were found and compared with the features extracted or selected by other common methods. All current work has focused only on selecting statistically significant features \cite{HuiZ20131}.

However, the accessibility and reliability of features used to describe vehicle driving conditions are normally quite different. In this work, 'Observability' is introduced to measure the accessibility and reliability of features. Observability of a feature means the difficulty in sampling/collecting the signal/data of this feature. For instance, it is easier to acquire instant vehicle speed using angular velocity sensors mounted on the driving shaft precisely, than to acquire the instantaneous engine power data. So the vehicle speed is therefore has better observability compared to engine power. Moreover, observability here also contains the concept of the reliability of obtained feature data. For instance, the real-time data on the State of Charge (SOC) of the HEV/PHEV's Energy Storage System (ESS) is typically estimated on-board by Kalman filters \cite{Di2008,Hongwen2011,HuiZ20132}. So SOC can be regarded as a feature with worse observability compared to vehicle speed. The concept of observability is therefore an important additional concept to statistical significance. However, in past and present research, the observability of data for the chosen features have never been taken into account. All feature data are treated equally, leading to less robust and efficient driving pattern recognition systems. In this work, an extended Support Vector Machine (SVM) with the capability for pattern discrimination and feature selection is developed. This extended SVM can choose features based on both the significance and observability of features.

\section{Feature selection for pattern recognition}

Two important factors in driving condition recognition are the feature selection, and the classification algorithm. The study in \cite{Ericsson2001} aims at finding independent features to describe the dimensions of urban driving conditions and to investigate which properties have main effect on emissions and fuel consumption. In \cite{Xi2011}, common methods for feature extraction, the Fast Fourier Transform (FFT), the Discrete Cosine Transform (DCT), and the Principal Component Analysis (PCA), were analyzed and compared against the four features found in this work. A detailed summary of features used to recognize driving cycles in the past several years can be found in \cite{Rui2011}.

In pattern recognition scenarios, Feature Subset Selection (FSS) typically picks out several significant features from the original $n$-dimension inputs (features), in order to facilitate data collection, reduce storage space and classification time and improve the accuracy of classification \cite{Neumann2005}. FSS mainly requires two parts, a feature search strategy to select candidate feature subsets and an objective function to evaluate the quality of the subset candidates.

Feature selection approaches can be divided into three categories: filters, wrappers and embedded approaches  \cite{Neumann2005}. Filters are the most known approaches, acting as a preprocessing step independently of the final classifier \cite{Hermes2000}. Filters usually evaluate feature subsets by their information content, typically using interclass distance, statistical dependence or information-theoretic measures. Filters generally involve a non-iterative computation on the original $n$-dimension features, which can execute very fast. However, since the filter objective functions are generally monotonic, the filter tends to select the full feature set as the optimal solution. In contrast, wrappers generally achieve better results than filters since they are tuned to the specific interactions between the classifier and the original feature set. In fact, wrappers take the classifier into account as a black box \cite{Weston2000}. However, since the wrapper must train a classifier for each feature subset (or several classifiers if cross-validation is used), the method can become unfeasible for computationally intensive methods. Finally, unlike filters and wrappers, embedded approaches simultaneously determine features and classifier during the training process \cite{Neumann2005}. The embedded methods in \cite{Bradley1998} are based on a linear classifier. Another embedded approach for SVM was suggested in \cite{Zhu2004}. More detailed discussion about the embedded approached for SVM can be found in the following section.

\section{Support Vector Machine with embedded feature selection}

Support Vector Machines (SVM), proposed by Vapnik and his group at AT\&T Bell Laboratories, are among the best "off-the-shelf" supervised learning algorithms. Given a two-class data set:
\begin{equation}
\bm{T} = \{(\bm{x}_1,y_1),\cdots, (\bm{x}_p,y_p)\}
\end{equation}
where, $\bm{x}_i\in\bm{R}^n, i = 1,2 \cdots p$ are $n$-dimension feature vectors, $y_i\in\{-1,1\}, i = 1,2,\cdots,p$ are class levels. Then the linear classifier with maximum margin, $y_i = sgn(\bm{\omega}^{T}\cdot\bm{x}_{i}+b), i = 1,2,\cdots,p$, can be found by solving the optimization problem as follows:
\begin{equation}
\begin{aligned}
&\min_{\bm{\omega},b}{\quad\frac{1}{2}\|\bm{\omega} \|_{2}^{2}}\\
\text{s.t.} \quad & y_{i}\cdot (\bm{\omega}^{T}\cdot\bm{x}_{i}+b)\geq 1, i=1,2,\ldots,p
\end{aligned}
\end{equation}
To make the algorithm work for non-linearly separable data sets as well as be less sensitive to outliers, the above optimization problem can be stated (using $l1$ regularization) as follows:
\begin{equation}
\label{standard}
\begin{aligned}
&\min_{\bm{\omega},b}{\quad\frac{1}{2}\|\bm{\omega} \|_{2}^{2}+C\cdot \sum_{i=1}^{p}{\xi_{i}}}\\
\text{s.t.} \quad & y_{i}\cdot (\bm{\omega}^{T}\cdot\bm{x}_{i}+b)\geq 1-\xi_{i}, i=1,2,\ldots,p\\
& \xi_{i}\geq 0, i=1,2,\ldots,p
\end{aligned}
\end{equation}
The parameter $C$ controls the relative weighting between the twin goals of making the $\|\bm{\omega} \|_{2}^{2}$ small and of ensuring that most training data have functional margin at least 1. After solving this optimization problem, the optimal $\bm{\omega}^{\star},b^{\star}$ can be found. For a new data point $\hat{\bm{x}}$, which is not in the training data set and is called testing data in this work, its group level $\hat{y}$ can be determined by just plugging this data point $\hat{\bm{x}}$ into $\hat{y} = sgn({\bm{\omega}^{\star}}^{T}\cdot\hat{\bm{x}}+b^{\star})$, where $sgn(\star)$ is a sign function, defined as:

\begin{equation}
  \begin{aligned}
  sgn(x) = \left\{
  \begin{array}{cc} 1 & x>0 \\ 0 & x=0 \\ -1 & x<0
  \end{array}
  \right.
  \end{aligned}
\end{equation}

In this work, the optimization problem (\ref{standard}) is called the standard 2-norm SVM (with soft margin). Furthermore, according to \cite{Vapnik1995}, it (\ref{standard}) can be also stated as:
\begin{equation}
\label{2norm}
\{\bm{\omega}^{\star},b^{\star}\} = arg \min_{\bm{\omega},b}{\sum_{i=1}^{p}{[1-y_{i}\cdot (\bm{\omega}^{T}\cdot\bm{x}_{i}+b)]_{+}}+\lambda\|\bm{\omega}\|_{2}^{2}}, i=1,2,\ldots,p
\end{equation}
where the subscript ``$+$'' means the positive part $(z_{+} = max(z, 0))$.

In 1998, a linear SVM with embedded feature selection was firstly proposed in \cite{Bradley1998} by measuring the margin distance using different vector norms. In 2004, another SVM with embedded feature selection was developed in \cite{Bhattacharyya2004}. This SVM was originally stated as
\begin{equation}
\begin{aligned}
&\min_{\bm{\omega},b}{\quad\|\bm{\omega} \|_{1}}\\
\text{s.t.} \quad & y_{i}\cdot (\bm{\omega}^{T}\cdot\bm{x}_{i}+b)\geq 1, i=1,2,\ldots,p
\end{aligned}
\end{equation}
Then in order to deal with the cases of non-linearly separable data sets, this optimization problem was manipulated into the form as follows:
\begin{equation}
\begin{aligned}
&\min_{\bm{\omega},b}{\quad \|\bm{\omega} \|_{1}}\\
\text{s.t.} \quad & Prob(\bm{x}_1\in\mathcal{H}_{1})\geq\eta\\
& Prob(\bm{x}_2\in\mathcal{H}_{2})\geq\eta\\
& \bm{x}_1\sim(\mu_{1},\sigma_{1}),\bm{x}_2\sim(\mu_{2},\sigma_{2})\\
\end{aligned}
\end{equation}
where, $\bm{x}_i\sim(\mu_{i},\sigma_{i})$ denotes a class of distributions that have mean $\mu_{i}$, and covariance $\sigma_{i}$,
The discriminating hyperplane of the SVM tries to place class $1$ in the half space $\mathcal{H}_{1}(\bm{\omega},b)=\{\bm{x}|\bm{\omega}^{T}\cdot\bm{x}>-b\}$ and class $2$ in the other half space $\mathcal{H}_{2}(\bm{\omega},b)=\{\bm{x}|\bm{\omega}^{T}\cdot\bm{x}<-b\}$. To ensure this, proper $\{\bm{\omega},b\}$ has to be found, such that $Prob(\bm{x}_1\in\mathcal{H}_{1})$ and $Prob(\bm{x}_2\in\mathcal{H}_{2})$ are both high. Finally, this optimization problem can be posed as a second-order cone programming (SOCP) by using a Chebyshev-Cantelli inequality and then solved. In the same year, \cite{Zhu2004} proposed the 1-norm SVM by replacing the 2-norm penalty in Equation \ref{2norm} with the 1-norm penalty.
\begin{equation}
\label{1norm}
\{\bm{\omega}^{\star},b^{\star}\} = arg \min_{\bm{\omega},b}{\sum_{i=1}^{p}{[1-y_{i}\cdot (\bm{\omega}^{T}\cdot\bm{x}_{i}+b)]_{+}}+\lambda\|\bm{\omega}\|_{1}}, i=1,2,\ldots,p
\end{equation}
In spite of the lack of oracle property \cite{zou2006}, 1-norm SVM has both good performance in feature selection and classification. In the next year, \cite{Neumann2005} summarized former works and used the Difference of Convex functions Algorithm (DCA) to solve the $l_{2}$-$l_{0}$-SVM, quadratic FSV and the kernel-target alignment approach. In 2007, an improved 1-norm SVM was proposed in \cite{Zou2007}. In 2010, \cite{Xu2010} introduced a 0-1 vector into the classification model of SVM
\begin{equation}
\begin{aligned}
&\min_{\bm{\omega},b}{\quad\frac{1}{2}\|\bm{\omega} \|_{2}^{2}+C\cdot \sum_{i=1}^{p}{\xi_{i}}}\\
\text{s.t.} \quad & y_{i}\cdot (\bm{\omega}^{T}\cdot\phi(\bm{z}\ast\bm{x}_{i})+b)\geq 1-\xi_{i}, i=1,2,\ldots,p\\
& \xi_{i}\geq 0, i=1,2,\ldots,p
\end{aligned}
\end{equation}
where $\bm{z}\ast\bm{x}_{i} = [z_{1}x_{i}^{1}, \cdots , z_{j}x_{i}^{j} ,\cdots, z_{n}x_{i}^{n}]^{T}$ is a vector and $z_{i} = 0$ or $1$. Although this is not a convex problem, it can be stated as a SOCP. However, all these works were not designed especially for driving pattern recognition in HEV applications and did not take observability of features into account. So an SVM with embedded feature selection and with respect to both statistical significance and observability of features is proposed in this work.

\section{Proposed algorithm}
\subsection{Embedded Feature-selection Support Vector Machine}
In order to embed feature selection into SVM, SVM is expected to use as few features as possible during classification. This can be expressed as follows:
\begin{equation}
\begin{aligned}
&\min_{\bm{\omega},b}{\quad\frac{1}{2}\|\bm{\omega} \|_{2}^{2}+C\cdot \sum_{i=1}^{p}{\xi_{i}}+T\cdot\|\bm{\omega}\|_{0}}\\
\text{s.t.} \quad & y_{i}\cdot (\bm{\omega}^{T}\cdot\bm{x}_{i}+b)\geq 1-\xi_{i}, i=1,2,\ldots,p\\
& \xi_{i}\geq 0, i=1,2,\ldots,p
\end{aligned}
\end{equation}
where $\|\bm{\omega}\|_{0}$ is the number of nonzero entries in $\bm{\omega}$. $T$ is the tuning parameter controlling the tradeoff between the performance of the SVM and the sparsity of $\|\bm{\omega}\|$. According to \cite{Chen2001}, adding this $L0$-norm penalty to the objective function is equivalent to adding an $L1$-norm LASSO penalty (least absolute shrinkage and selection operator) \cite{LASSO94}. Moreover, in order to take observability of each feature into account, a weight vector is introduced into the LASSO penalty of the objective function. After slight manipulation, the SVM with embedded feature selection proposed in this work can be finally stated as :
\begin{equation}
\begin{aligned}
\label{ori}
&\min_{\bm{\omega},b}{\quad K\cdot\|\bm{\omega} \|_{2}+C\cdot \sum_{i=1}^{p}{\xi_{i}}+T\cdot\|\bm{z}\ast\bm{\omega}\|_{1}}\\
\text{s.t.} \quad & y_{i}\cdot (\bm{\omega}^{T}\cdot\bm{x}_{i}+b)\geq 1-\xi_{i}, i=1,2,\ldots,p\\
& \xi_{i}\geq 0, i=1,2,\ldots,p
\end{aligned}
\end{equation}
here, $\bm{z}\ast\bm{\omega} = [z_{1}\omega_{1}, \cdots , z_{j}\omega_{j},\cdots , z_{n}\omega_{n}]^{T}$. $z_{i} \in (0,1]$. For a small $z_{i}$, the observability of $i$th feature is considered good. $K$, $C$ and $T$ are all tuning parameters and $K$ is always set to be $1$. If $T$ is zero, then this extended SVM is a standard 2-normSVM (with soft margin). If $K$ are all zeros and $\bm{z}$ is a vector having all ``$1$'' entries, then this extended SVM is:
\begin{equation}
\begin{aligned}
&\min_{\bm{\omega},b}{\quad C\cdot \sum_{i=1}^{p}{\xi_{i}}+T\cdot\|\bm{\omega}\|_{1}}\\
\text{s.t.} \quad & y_{i}\cdot (\bm{\omega}^{T}\cdot\bm{x}_{i}+b)\geq 1-\xi_{i}, i=1,2,\ldots,p\\
& \xi_{i}\geq 0, i=1,2,\ldots,p
\end{aligned}
\end{equation}
which is equivalent to a 1-norm SVM as shown in Equation \ref{1norm}. So the conclusion can be drawn that standard 2-norm SVM (with soft margin) and 1-norm SVM are both special cases of this extended SVM, when the tuning parameters and weight vector $\bm{z}$ have particular values. More detailed comparison among these three SVMs can be found in the simulation section.

\subsection{SVM optimization approach}

The optimization problem of the proposed SVM is solved by using Second-order Cone Programming (SOCP). In a SOCP, a linear function is minimized over the intersection of an affine set and the product of second-order (quadratic) cones. SOCPs are nonlinear convex problems that include linear and (convex) quadratic programs as special cases, but are less general than Semidefinite Programs (SDPs). \cite{Lobo1998}. The relation among Linear Programming (LP), (convex) Quadratic Programming (QP), SOCP, SDP and Convex Programming (CP) can be found in Figure \ref{relation} \cite{BookLu2007}.
\begin{figure}[htbp]
\center
\includegraphics [width=0.5\textwidth, height = 0.3\textwidth]{chapters/4/images/relation.eps}
\caption{Relation among LP, convex QP, SOCP,SDP and CP}
\label{relation}
\end{figure}

A typical SOCP problem can be stated as follows:
\begin{equation}
\begin{aligned}
&\min_{\bm{x}}{\quad \bm{f}^{T}\cdot\bm{x}}\\
\text{s.t.} \quad & \|\bm{A}_{i}\bm{x}+\bm{b}_{i}\|\leq\bm{c}_{i}^{T}\bm{x}+d_{i}, \quad i=1,2,\ldots,M
\end{aligned}
\end{equation}
where $\bm{x} \in \bm{R}^{n}$ is the optimization variable. The problem parameters are $\bm{f} \in \bm{R}^{n}$,
$\bm{A}_{i} \in \bm{R}^{(n_{i}-1)\times n}$, $\bm{b}_{i} \in \bm{R}^{n_{i}-1}$, $\bm{c}_{i} \in \bm{R}^{n}$, and
$d_{i} \in \bm{R}$. The norm appearing in the constraints is the standard Euclidean norm.

The extended SVM, Equation \ref{ori}, can be proved to be a standard SOCP problem as follows:

\begin{proof}
Two auxiliary variables $\delta$ and $\bm{\eta}$ are introduced into Equation \ref{ori}. $\delta$ is the upper bounds of $\|\bm{\omega}\|^{2}$ and $\bm{\eta}$ is a $n$-dimensional vector, $|\omega^{i}| \leq \eta^{i}$ and $\omega^{i}$ means the $i$th entry of vector $\bm{\omega}$ and $\eta^{i}$ means the $i$th entry of vector $\bm{\eta}$. After introducing these two auxiliary variables, the optimization variable in Equation \ref{ori} can be changed to $\hat{\bm{\omega}} = \left[\begin{array}{ccccc}\bm{\omega}^{T}&\bm{\eta}^{T}&\xi_{1} \cdots \xi_{p}& b&\bm{\delta}^{T}\end{array}\right]^{T}$. By using the new variable $\hat{\bm{\omega}}$, the constraints in Equation \ref{ori} can be stated as follows:
\begin{equation}
\begin{aligned}
&\left[ \begin{array}{ccccc}
  y_{1}\bm{x}_{1} & \bm{0}_{(1\times n)} & \bm{u}_{(1\times p)}(1) & y_{1} & 0\\
  y_{2}\bm{x}_{2} & \bm{0}_{(1\times n)} & \bm{u}_{(1\times p)}(2) & y_{2} & 0\\
   & & \vdots & &\\
  y_{p}\bm{x}_{p} & \bm{0}_{(1\times n)} & \bm{u}_{(1\times p)}(p) & y_{p} & 0
  \end{array}\right]\cdot \hat{\bm{\omega}}-\bm{e}_{(p \times 1)}\geq \bm{0}_{p\times1}\\
&\left[ \begin{array}{ccccc}
  \bm{0}_{(p\times n)} & \bm{0}_{(p\times n)} & \bm{I}_{(p \times p)} & \bm{0}_{(p \times 1)} & \bm{0}_{(p \times 1)}
  \end{array}\right]\cdot \hat{\bm{\omega}}-\bm{0}_{(p \times 1)}\geq \bm{0}_{p\times1}
\end{aligned}
\end{equation}
here, $\bm{u}_{(1\times p)}(i)$ is a $1 \times p$ vector, whose $i$th entry is $+1$ and all other entries are $0$,
$\bm{I}_{(p \times p)}$ is a $p \times p$ identity matrix, and $\bm{e}_{(p \times 1)}$ is a $p \times 1$ vector having all $1$ entries. Moreover, these two constraints can be combined and manipulated into a standard line cone as follows:
\begin{equation}
  \begin{aligned}
  \label{first}
  \bm{D}^{T}\hat{\bm{\omega}}+\bm{f} \geq \bm{0}_{2p\times1}
  \end{aligned}
  \end{equation}
  where
  \begin{equation}
  \begin{aligned}
  &\bm{D}^{T} = \left[ \begin{array}{c} \bm{D}_{1}\\\bm{D}_{2}\end{array}\right]_{(2p\times(2n+p+2))}\\
  &\bm{D}_{1} = \left[ \begin{array}{ccccc}
  y_{1}\bm{x}_{1} & \bm{0}_{(1\times n)} & \bm{u}_{(1\times p)}(1) & y_{1} & 0\\
  y_{2}\bm{x}_{2} & \bm{0}_{(1\times n)} & \bm{u}_{(1\times p)}(2) & y_{2} & 0\\
   & & \vdots & &\\
  y_{p}\bm{x}_{p} & \bm{0}_{(1\times n)} & \bm{u}_{(1\times p)}(p) & y_{p} & 0
  \end{array}\right]_{p\times(2n+p+2)}\\
  &\bm{D}_{2} = \left[ \begin{array}{ccccc}
  \bm{0}_{(p\times n)} & \bm{0}_{(p\times n)} & \bm{I}_{(p \times p)} & \bm{0}_{(p \times 1)} & \bm{0}_{(p \times 1)}
  \end{array}\right]_{p\times(2n+p+2)}\\
  & \bm{f} = \left[ \begin{array}{c} -\bm{e}_{(p \times 1)}\\\bm{0}_{(p \times 1)}\end{array}\right]_{2p\times1}
  \end{aligned}
\end{equation}
However, because of introducing two auxiliary variables, two more sets of constraints should be added to Equation \ref{ori}. They are:

\begin{equation}
\label{con1}
\|\bm{\omega}\|_{2}\leq\delta
\end{equation}

\begin{equation}
\label{con2}
|\omega^{i}| \leq \eta^{i}, i=1,2,\ldots,n
\end{equation}
These two sets of constraints can be manipulated into standard second-order cones. For Equation \ref{con1}, this constraint can be stated as follows:
\begin{equation}
  \begin{aligned}
  \|\bm{A}_{1}^{T}\hat{\bm{\omega}}+\bm{c}_{1}\|_{2} \leq \bm{b}_{1}^{T}\hat{\bm{\omega}}+d_{1}
  \end{aligned}
\end{equation}
where
\begin{equation}
  \begin{aligned}
  &\bm{A}_{1}^{T} = \left[ \begin{array}{ccccc}
  \bm{I}_{(n \times n)} & \bm{0}_{(n\times n)} & \bm{0}_{(n \times p)} & \bm{0}_{(n \times 1)} & \bm{0}_{(n \times 1)}
  \end{array}\right]_{n\times(2n+p+2)}\\
  &\bm{b}_{1} = \left[ \begin{array}{c} \bm{0}_{((2n+p+1) \times 1)}\\1\end{array}\right]\\
  &\bm{c}_{1} = \bm{0}_{n\times1}, \quad d_{1} = 0
  \end{aligned}
\end{equation}
For Equation \ref{con2}:
\begin{equation}
  \begin{aligned}
  \|\bm{A}_{i+1}^{T}\hat{\bm{\omega}}+\bm{c}_{i+1}\|_{2} \leq \bm{b}_{i+1}^{T}\hat{\bm{\omega}}+d_{i+1},  i=1,2,\ldots,n
  \end{aligned}
\end{equation}
where
\begin{equation}
  \begin{aligned}
  \label{last}
  &\bm{A}_{i+1}^{T} = \left[ \begin{array}{ccccc}
  \bm{u}_{(1\times n)}(i) & \bm{0}_{(1\times n)} & \bm{0}_{(1 \times p)} & 0 & 0
  \end{array}\right]_{1\times(2n+p+2)}, i=1,2,\ldots,n\\
  &\bm{b}_{1}^{T} = \left[ \begin{array}{ccccc}
  \bm{0}_{1\times n} & \bm{u}_{(1\times n)}(i) & \bm{0}_{(1 \times p)} & 0 & 0
  \end{array}\right]_{1\times(2n+p+2)}, i=1,2,\ldots,n\\
  &c_{i+1} = d_{i+1} = 0, i=1,2,\ldots,n
  \end{aligned}
\end{equation}


Here, $\bm{u}_{(1\times n)}(i)$ is a $1 \times n$ vector, whose $i$th entry is $+1$ and all other entries are $0$. Finally, Equation \ref{ori} can be stated as a standard SOCP problem as follows:
\begin{equation}
\begin{aligned}
&\min_{\hat{\bm{\omega}}}{\quad \bm{b}^{T}\hat{\bm{\omega}}}\\
\text{s.t.}\quad
& \bm{D}^{T}\hat{\bm{\omega}}+\bm{f} \geq \bm{0}_{p\times1}\\
& \|\bm{A}_{1}^{T}\hat{\bm{\omega}}+\bm{c}_{1}\|_{2} \leq \bm{b}_{1}^{T}\hat{\bm{\omega}}+d_{1}\\
& \|\bm{A}_{i+1}^{T}\hat{\bm{\omega}}+\bm{c}_{i+1}\|_{2} \leq \bm{b}_{i+1}^{T}\hat{\bm{\omega}}+d_{i+1}\\
& i=1,2,\ldots,n
\end{aligned}
\end{equation}
where, $\bm{b} = \left[\begin{array}{ccccc}\bm{0}_{(1\times n)}&T\cdot\bm{z}_{(1\times n)}&c\cdot\bm{e}_{(1\times p)}& 0&1\end{array}\right]^{T}$, $\bm{z}_{(1\times n)}$ here is the weight vector. All other detailed optimization parameters can be found from Equations \ref{first}-\ref{last}.
\end{proof}

Moreover, all the equations above are written in accordance with the notation of SeDuMi, an MATLAB toolbox for optimization written by Jos F. Sturm in 1999, so that this extended SVM can be solved efficiently by SeDuMi.

\section{Simulation results}

\subsection{Comparison with standard 2-norm SVM and 1-norm SVM by using ``Ionosphere'' data set}
In this section, the proposed SVM, standard 2-norm SVM and 1-norm SVM are compared by using the widely adopted data set ``Ionosphere'', provided by UCI machine learning repository(http://archive.ics.uci.edu/ml /datasets.html) \cite{Bache2013}. Each pattern in this data set has $34$ features, which means adequate different feature combinations can be provided. Therefore, the performance of feature selection can be compared by using this data set. Note that in order to keep the fairness of the comparison among different SVMs, the weight vector $\bm{z}$ (in the proposed SVM) of the observability of each feature is set to have all $1$ entries in this section. Finally, to give a more realistic estimation of generalization, a five-fold cross-validation is used to compare the performance of all SVMs. The classification error and the number of non-zero entries in $\bm{\omega}$, which are shown in the following figures, are the mean values of the results of the five-fold cross-validation.

First, the comparison between the proposed SVM and standard 2-norm SVM can be found in Figure \ref{com2}. The tuning parameter $K$ in the proposed SVM is always set to be $1$, and $C$s of both SVMs keep constant at $1$, too. With increasing tuning parameter $T$, the proposed SVM succeeds in selecting fewer features for classification. However, the classification error also goes up. When $T$ increases to $10$, only 11 features are used for classification and the classification error only rises to $14\%$ from $11\%$. When $T$ is zero, the performance of the proposed SVM and standard 2-norm SVM are identical. Compared to standard 2-norm SVM, the proposed SVM has obviously higher efficiency by using many fewer features for classification at the cost of acceptable decrease of classification accuracy. In Figure \ref{std2norm}, standard deviation of both non-zeros entries in $\bm{w}$ and classification error of these two types of SVMs are illustrated. An important note should be made here: since the standard 2-norm SVM does not have parameter $T$, tuning $T$ has no effect on the performance of standard 2-norm SVM. Therefore the performance curves of standard 2-norm SVM are all constant horizontal lines in all of the figures with horizontal axis of parameter $T$.

Next, the proposed SVM is compared with 1-norm SVM. As shown in Figure \ref{norm11}, the performance of these two SVMs are almost the same. However, from Figure \ref{norm12}, the proposed SVM can provide more tuning options during the design of pattern recognition system by tuning $C$ and $T$ simultaneously.

\begin{figure}[htb]
\subfloat[]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/T_err_com2.eps}}
\subfloat[]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/T_nonzeros_com2.eps}}\\
\caption{Comparison of the proposed SVM and standard 2-norm SVM}
\label{com2}
\end{figure}

\begin{figure}[htb]
\subfloat[Standard deviation of classification error]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/std2normerr.eps}}
\subfloat[Standard deviation of non-zeros entries in $\bm{w}$]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/std2normw.eps}}\\
\caption{Standard deviation of the five-fold cross-validation: 2-norm SVM vs. the proposed SVM}
\label{std2norm}
\end{figure}

\begin{figure}[htb]
\subfloat[]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/Error_norm1_T.eps}}
\subfloat[]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/Nonzero_norm1_T.eps}}\\
\caption{Comparison of the proposed SVM and 1-norm SVM}
\label{norm11}
\end{figure}

\begin{figure}[htb]
\subfloat[Standard deviation of classification error]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/std1normerr.eps}}
\subfloat[Standard deviation of non-zeros entries in $\bm{w}$]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/std1normw.eps}}\\
\caption{Standard deviation of the five-fold cross-validation: 1-norm SVM vs. the proposed SVM}
\label{std1norm}
\end{figure}


\begin{figure}[htb]
\subfloat[]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/error2_3d.eps}}
\subfloat[]{\includegraphics[height=0.4\textwidth,width=0.49\linewidth]{chapters/4/images/sparse2_3d.eps}}\\
\caption{Performance of the proposed SVM}
\label{norm12}
\end{figure}

\subsection{Testing by using the data collected from the simulation results of Toyota Prius}


In this section, the proposed SVM is tested and compared with standard 2-norm SVM and 1-norm SVM by using the data collected from the simulation results of a Toyota Prius. The model of the Toyota Prius HEV is built in Autonomie, a vehicle simulation tool developed by US DOE Argonne National Laboratory. The Toyota Prius is a hybrid electric mid-size hatchback with power split powertrain architecture. There are two electric motors and one engine in its powertrain and the M/G 1 is coupled with the final drive directly.

First of all, in order to generate the data set for training and testing the SVMs, the model of the Prius is run in four different types of driving cycles. These cycles are highway, congested urban road, flowing urban road and country road driving cycles, which can be found in Figure \ref{cycles}. Based on \cite{Rui2011}, $12$ real-time vehicle state variables (features) are collected from the simulation results to form four data subsets, $Data_{1}$ collected from highway simulation, $Data_{2}$ from congested urban, $Data_{3}$ from  flowing urban and $Data_{4}$ from country road. These $12$ features are listed in Table (\ref{features}). So the entire data set in this section is $DATA = Data_{1}\cup Data_{2}\cup Data_{3}\cup Data_{4}$. Furthermore, the observability values, $z_{i}\in(0,1]$ are assigned to each feature as shown in Table (\ref{features}). According to \cite{Yang2008}, the characteristic of the current driving condition can be effectively recognized, if the length of the sampling time reaches or exceeds $3$ minutes. So all these features are sampled and calculated over every $3$ minutes. After collecting the data set $DATA = Data_{1}\cup Data_{2}\cup Data_{3}\cup Data_{4}$ from the simulation, a five-fold cross-validation is also used to test the performance of the proposed SVM. All the results shown in the following figures are also the mean values of the results of the five-fold cross-validation.


\begin{figure}[htb]
\subfloat[Highway.]{\includegraphics[height=0.19\textwidth,width=0.485\linewidth]{chapters/4/images/4a.eps}}
\subfloat[Congested urban road.]{\includegraphics[height=0.19\textwidth,width=0.485\linewidth]{chapters/4/images/4b.eps}}\\
\subfloat[Country road.]{\includegraphics[height=0.19\textwidth,width=0.485\linewidth]{chapters/4/images/4c.eps}}
\subfloat[Flowing urban road.]{\includegraphics[height=0.19\textwidth,width=0.485\linewidth]{chapters/4/images/4d.eps}}
\caption{Speed signals for four different driving conditions, sampled at $1Hz$}
\label{cycles}
\end{figure}

\begin{table}[htb]
\caption[Featue set for the Extended Support Vector Machine]{Feature set: ``OBS'' means the Observability value for that feature}
\begin{center}
\label{features}
\begin{tabular}{|c|l|c|c|l|c|}
\hline
FN& Name & OBS & \# & Name & OBS \\
\hline
1  & Average vehicle speed & 0.1 & 7 & Average engine speed & 0.15 \\
\hline
2  & Maximal vehicle acceleration & 0.3 & 8 & Average engine torque & 0.15\\
\hline
3  & Maximal vehicle deceleration & 0.3 & 9 & Average current in Motor 1 & 0.35\\
\hline
4  & Idle rate of vehicle speed & 0.2 & 10 & Average speed of Motor 1 & 0.15\\
\hline
5  & Change of battery SOC & 0.5 & 11 & Average current in Motor 2 & 0.35\\
\hline
6  & Average driver demanded power & 0.4 & 12 & Average speed of Motor 2 & 0.15\\
\hline
\end{tabular}
\end{center}
\end{table}

\subsubsection{Two-class driving condition recognition}

In this section, the data subsets: $Data_{1}$, $Data_{2}$, $Data_{3}$ and $Data_{4}$ are combined in various pairs for training and testing the proposed SVM. The combinations used in this section are highway vs. congested urban road, flowing urban road vs. congested urban road, flowing urban road vs. country road, and highway vs. country road. Standard 2-norm SVM, 1-norm SVM and the proposed SVM are trained and tested by these four 2-class data set. Moreover, the proposed SVM are trained and tested in two modes, \emph{Mode 1}: taking observability into account and \emph{Mode 2}: without consideration of observability (by setting observability of each feature as $1$). The results can be found in Figure \ref{recomf}. As shown in these figures, the proposed SVM can find a relatively better balance between the number of selected features and the accuracy of classification. Standard 2-norm SVM has the best accuracy of classification, however, it does not have the ability for feature selection. On the other hand, 1-norm SVM can select the fewest features, while the classification accuracy is the worst. Note should be made here that the parameter $T$ in 1-norm SVM cannot be set to 0, so the performance curves of 1-norm SVM in these figures start from $T = 1$. Moreover, in order to demonstrate the superiority of taking observability into account, the features selected by the proposed SVM in Mode $1$ (taking observability into account) are compared with those features selected by the proposed SVM in Mode $2$ (without consideration of observability). The selected features by both modes are listed in Table (\ref{recom}). The symbol '$\star$' denotes a feature selected by the SVM in Mode $1$ and $\circ$ means this feature selected by Mode $2$ . Furthermore, the total observability value of the features selected by both modes are calculated and listed in the right side in Table (\ref{recom}). Obviously, the proposed SVM with observability consideration can select more reliable and accessible features so that higher recognition efficiency can be achieved.

\begin{figure}[htb]
\subfloat[Highway vs. Congested urban]{\includegraphics[height=0.19\textwidth,width=0.485\linewidth]{chapters/4/images/hc4.eps}}
\subfloat[Flowing urban vs. Congested urban]{\includegraphics[height=0.19\textwidth,width=0.485\linewidth]{chapters/4/images/fccity4.eps}}\\
\subfloat[Country road vs. Flowing urban]{\includegraphics[height=0.19\textwidth,width=0.485\linewidth]{chapters/4/images/contf4.eps}}
\subfloat[Highway vs. Country road]{\includegraphics[height=0.19\textwidth,width=0.485\linewidth]{chapters/4/images/ch4.eps}}
\caption{Comparison between with observability consideration and no observability consideration}
\label{recomf}
\end{figure}

\begin{table}[htb]
\caption[Comparison of selected features]{Comparison of selected features: FN-Feature Number; OBS-Observability Value; $\star$ is the feature selected by the proposed SVM in mode $1$ (with observability consideration); $\circ$ is the feature selected by the proposed SVM in mode $2$ (without observability consideration)}
\begin{center}
\label{recom}
\small
\setlength\tabcolsep{5pt}
\begin{tabular}{|c | c| c| c| c| c| c| c| c| c| c| c| c|| c| c|}
\hline
FN& 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12& \multicolumn{2}{c|}{Total OBS} \\
\hline
OBS&0.1&0.3&0.3&0.2&0.5&0.4&0.15&0.15&0.3&0.15&0.3&0.15&$\star$&$\circ$ \\
\hline
\multicolumn{15}{|c|}{Highway vs. Congested Urban Road}\\
\hline
T=0&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&3 &3 \\
\hline
T=5&$\star$ $\circ$& & & &$\circ$& &$\star$&$\star$ $\circ$& &$\star$ $\circ$& &$\star$ $\circ$&0.7 &1.05 \\
\hline
T=10&$\star$& & & &$\circ$& & &$\star$ $\circ$& &$\star$& &$\star$ $\circ$&0.55 &0.8 \\
\hline
\multicolumn{15}{|c|}{Flow Urban Road vs. Congested Urban Road}\\
\hline
T=0&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&3 &3 \\
\hline
T=5&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\circ$& &$\star$ $\circ$&$\star$ $\circ$& &$\star$ $\circ$& &$\star$ &1.5 &1.85 \\
\hline
T=10&$\star$ &$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$& $\circ$& &$\star$ $\circ$&$\star$ & &$\star$& & &1.35 &1.45 \\
\hline
\multicolumn{15}{|c|}{Flow Urban Road vs. Country Road}\\
\hline
T=0&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&3 &3 \\
\hline
T=5&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$& & & &$\star$ $\circ$& & $\circ$& & &1.05 &1.2 \\
\hline
T=10&$\star$ $\circ$& &$\star$ $\circ$&$\star$ $\circ$& & & &$\star$& &$\circ$& & &0.75 &0.75 \\
\hline
\multicolumn{15}{|c|}{Highway vs. Country Road}\\
\hline
T=0&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&$\star$ $\circ$&3 &3 \\
\hline
T=5&$\star$&$\star$ & & & $\circ$& & &$\star$  $\circ$& $\circ$&$\star$ & &$\star$  $\circ$&0.85 &1.1 \\
\hline
T=10&$\star$& & & &$\star$ $\circ$& & &$\star$ $\circ$&$\circ$& & &$\star$&0.9 &0.95 \\
\hline
\end{tabular}
\end{center}
\end{table}


\subsubsection{Multi-class driving condition recognition}
In this section, the entire $4$-class data set, $DATA = Data_{1}\cup Data_{2}\cup Data_{3}\cup Data_{4}$, is used to train and test the proposed SVM for multi-class pattern recognition. In this work, the multi-class SVM is based on ``Max WIN Voting'' strategy (MWV-SVM). For the four driving conditions scenario, $6$ ($C(4,2)$) the proposed SVMs are used for classifying current driving pattern into each of the two driving conditions. If an SVM classifies the current driving pattern into the $i$th driving condition, then the vote for the $i$th driving condition is incremented by one. Finally, the current driving pattern is predicted to be the driving condition with largest vote. The selected feature set of $\mathscr{F}$ in the multi-class SVM is determined as follows:
\begin{equation}
\begin{aligned}
\mathscr{F} = \bigcup_{i=1}^{6}f_{i}
\end{aligned}
\end{equation}
where, $f_{i}$ is the feature subset selected by each of the six proposed SVM. The experiment results can be found in Figure \ref{all12}, where the proposed SVM are also compared with two other SVMs and obviously also has a better balance between the amount of selected features and the accuracy of classification. Note should be made here that the parameter $T$ in 1-norm SVM cannot be set as 0, so the curves of 1-norm SVM in these figures start from $T = 1$.
\begin{figure}[htb]
\subfloat[]{\includegraphics[height=0.4\textwidth,width=0.485\linewidth]{chapters/4/images/all1.eps}}
\subfloat[]{\includegraphics[height=0.4\textwidth,width=0.485\linewidth]{chapters/4/images/all2.eps}}\\
\caption{Comparison among three SVMs on multi-class data set}
\label{all12}
\end{figure}
