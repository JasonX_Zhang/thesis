\startfirstchapter{Introduction}
\label{chapter:introduction}

% **************************** Section 1.1 ***************************
\section{Background}

Today, increasing environmental concerns and dwindling petroleum reserves demand alternative energy solutions for the dominating energy consuming transportation sector, especially ground vehicles. Meanwhile the total number of vehicles will increase from 700 million to 2.5 billion over the next 50 years globally, intensifying the problem \cite{chan_wong_2004}. Further improvement of fuel economy and finding alternative or renewable energy sources for vehicle propulsion have been the focus of vehicle propulsion technology development worldwide.

Many advanced technologies for reducing petroleum consumption and tailpipe emissions have been introduced. One straightforward approach is to improve powertrain efficiency and lower vehicle resistance forces. This path can be further divided into four major areas of technology improvements: engine, transmission, vehicle and hybrid techniques. A comprehensive survey on these techniques can be found in \cite{epa}. Hybridization of powertrain is widely considered as a practical and effective solution to significantly improve ICE efficiency and emissions in near future \cite{eia}. Hybrid vehicle (HV) is defined as a vehicle with two or more energy storage system (ESS), both of which must provide propulsion power–either together or independently \cite{sae_report}. Specifically, in addition to conventional fuel tank, the secondary ESS could be flywheel, compressed air tank, battery, ultracapacitor (UC) as well as combination of battery-ultracapacitor, as summarized in right-bottom block of Figure \ref{c1_1} \cite{john_miller_2004} \cite{x_wang_2008} \cite{e_tanaka_2013}. These types of HVs differ from each other greatly from operation principle, performance and fuel efficiency (FE) benefits as well as costs. Today, HVs equipped with battery as ESS are in a monopoly position in numbers and in types, in comparison with other alternatives. The second path for reducing petroleum consumption is to shift the use of petroleum fuel to other energy sources. Various alternative energy sources and corresponding powertrains are summarized at two sides of Figure \ref{c1_1}, according to energy sources, on-board energy and propulsion systems. Primary solutions include electrified vehicles (EV), including Pure Electric Vehicle (PEV) and Plug-in Hybrid Electric Vehicle (PHEV), and flexible-fuel vehicle (FFV). 

% ============== figure 1.1 ==============
\begin{figure}[hbt]
  \center
  \includegraphics{chapters/1/images/1.png}
  \caption{Alternative fuel and powertrain solutions}
  \label{c1_1}
\end{figure}
% ========================================

% --------------- Section 1.1.1 -----------------
\subsection{Electrified Vehicles}
Although flexible-fuel vehicle (FFV) will continue expanding market penetration, electrified vehicles (EV) will be the most practical and influential choice in the following decades for a couple of reasons: a) electric energy is pivotal element for diversification of energy sources, beneficial for energy security; b) petroleum will continue to be primary fuel of on-land vehicles in decades, so hybridization of vehicle will play a critical role in improving mass-production vehicle efficiency and reducing emissions; c) HEVs, particularly PHEVs, shown at left-bottom corner of Figure \ref{c1_1} is intersection of electrification and hybridization approaches, providing a wide range of technical solutions \cite{eia, epa}. Electrified vehicles, especially HEVs and PHEVs, combine hybridization and electrification, possessing special potential.

The level of electrification of all EVs has great impact on powertrain architecture design and selection, further dividing all EVs into five sub-categories: micro HEV, mild HEV and strong HEV, PHEV, Extended Range Electric Vehicle (EREV), PEV and Fuel Cell Electric Vehicle (FCEV), forming the family of xEVs. Powertrain architecture, which refers to layout and energy flow paths among key powertrain components, is an important index of various types of HEVs. Many scholars have categorized HEV/PHEV into Series Hybrid, Parallel Hybrid and Power-split Hybrid, with emphasizes on its internal combustion engine (ICE), Electric Machine (EM) or electric motors/generators (M/Gs), power electronics, and their modeling \cite{john_miller_2004} \cite{a_emadi_2005} \cite{m_ehsani_2007} \cite{c_c_chan_2010} \cite{m_ehsani_2005}. 

The first appearance of EVs or PEVs dates back to the early 1830. These EVs were not commercial vehicles as they used non-rechargeable batteries. It will take an additional half a century before batteries are developed sufficiently to be used in commercial vehicles \cite{larminie}. In 1989 Ferdinand Porsche, an employee of the Austrian company Jacob Lohner \& Co, developed a drive system based on fitting an electric motor to each front wheel, without using a transmission \cite{web_4}. During the $20$th century petroleum powered vehicles showed absolute dominance over the EVs. The reasons are easily understood when the specific energy of petroleum fuel is compared to that of batteries. For example, the specific energy of diesel, i.e. energy stored per kilogram, is about $12600$ $Wh/kg$, while the highest reported specific energy of Lithium-air batteries is about $360$ $Wh/kg$ \cite{kraytsberg, zhang}. Moreover, the diesel is much cheaper with $0.15$ $e/kWh$, compared to the optimistic price of about $180$ $e/kWh$ for energy optimized batteries projected by the United States Advanced Battery Consortium \cite{linda}.

Nevertheless, the electrification of vehicles has increased again in the 21st century motivated by the air pollution, global warming and rapid depletion of petroleum resources. Obviously, in order to develop efficient and cost effective EVs, one of the key technical challenges and bottle necks needed to be improved or solved in vehicle electrification is the high performance, reliable, long-lasting and low-cost on board electrical energy storage system (ESS).

% --------------- Section 1.1.2 -----------------
\subsection{Electrical energy storage system}

As stated previously, the powertrain system of a HEV/PHEV could be categorized into series, parallel or power-split types depending upon how power from its ICE and M/Gs are blended, and EVs can be classified into micro, mild and strong HEV, PHEV, EREV, PEV or FCEV based on its electrification level. However, no matter in what category an EV is, it always needs a reliable and efficient ESS to store electrical energy.

The most common ESS used in EVs is the battery pack. Batteries have been the technology of choice for most EV applications, due to its capability of storing large amounts of energy in a relatively small volume and light weight, referring to high volumetric and mass energy density, and providing suitable levels of power for many applications. 

Shelf and cycle life have been a concern with most types of batteries, but people have learned to adapt to this shortcoming due to the lack of an alternative. In recent years, the increased level of electrification led to the sharply increased power requirements from a number of EV applications, exceeding the capability of batteries of standard design \cite{a_burke2014}. This demand has led to the design of special high power, pulse batteries often with the sacrifice of energy density and cycle life. Ultracapacitor (UC) have been developed as an attractive alternative to pulse batteries with an order of magnitude longer shelf and cycle life \cite{a_burke2014}.


In order to improve the efficiency, performance, cost and life of on-board electrical ESS, appropriate integration of two or more energy storage means has been researched to allow the best characteristics of each type to be fully utilized, leading to a hybrid energy storage system (HESS). Since battery alone cannot serve as the optimal energy source for all EVs due to their power and energy trade-offs, as shown in the Ragone plot of Figure \ref{c1_2} \cite{electropaedia}. Combining batteries and ultracapacitors can create an ESS with both high peak power and high energy density.

% ============== figure 1.2 ==============
\begin{figure}[hbt]
  \center
  \includegraphics{chapters/1/images/2.png}
  \caption{Ragone plot}
  \label{c1_2}
\end{figure}
% ========================================

% **************************** Section 1.1 ***************************
\section{Research contributions}
A HESS is able to provide EVs with both high peak power and high energy density, and to provide an ESS solution with extended life, using its UCs to reduced frequent charge/discharge on batteries. However, the HESS requires effective operation coordination of the battery and ultracapacitor within the HESS by its EMS. 

This research mainly focuses on the development of intelligent control algorithms to perform the coordination by optimizing their energy flow. The newly proposed intelligent energy management method and algorithm are able to produce quasi-optimal control solutions adapted to different driving trips. To better support this driving trip-based intelligent energy flow control method, two new driving pattern recognition algorithms that improve automated feature selection and dynamic pattern recognition capability have also been introduced. Combination of these three new methods and algorithms supports the effective energy allocation between battery and ultracapacitor in the HESS based on the pre-calculated optimal control plan associated a specific driving pattern that has been recognized during previous driving trips and has been matched using driving data in real-time.  The newly proposed algorithms have been validated through simulations with proved performance improvements. Detailed research contributions are listed as follows:

\begin{enumerate}
  \item The operations of both of battery and ultracapacitor involve complex electrochemical and electrothermal processes. In order to carry out Reinforcement Learning (RL) from the HESS data, to simulate and to validate the performance and effectiveness of the newly proposed algorithms, a Matlab-Python combined simulation method has been adopted in this work. With this special simulation method, training data sets for obtaining the network parameters inside the introduced Intelligent Energy Allocation algorithm are obtained, and validations on the effectiveness of the new algorithms have been carried out.

  \item The system efficiency of three major topologies of battery-ultracapacitor HESS has been evaluated and compared using a typical pulsed current load, and has been published in \cite{self1}. Based on this study, the semi-active HESS topology with an ideal DC-DC converter on the ultracapacitor side has been selected in the following research. 

  \item For the selected HESS structure, an intelligent energy allocation algorithm (IEA) employing a reinforcement learning has been introduced. This new algorithm is able to adapt itself to different daily driving modes, and keep learning to generate optimal control strategies/policies to prolong battery life and improve HESS efficiency using the data collected during driving. This nature of the IEA led to the fact that the more the car is driven, the higher the HESS efficiency.  To better facilitate the identification of driving modes from daily driving using the new IEA algorithm, two other key algorithms have been introduced as additional research contributions.

  \item A more efficient and robust driving pattern recognition technique, extended Support Vector Machine (e-SVM) with embedded feature selection ability, is proposed in this work. Besides statistical significance, this proposed SVM also takes into account the accessibility and reliability of features during feature selection, so as to enable the driving condition discrimination system to achieve higher recognition efficiency and robustness. This work was published in the Journal of Franklin Institute \cite{self2}. This work helps to obtain a higher accuracy of the driving pattern recognition, hence the better performance of the IEA algorithm.

  \item A novel classification algorithm, Inertial Matching Pursuit Classification (IMPC), is also proposed in this work. Compared with the traditional methods using SVM or Neural Networks, IMPC can recognize the diving patterns using vehicle velocity data sampled in less sampling time, so the accuracy for estimating the overall driving conditions of the entire trip is improved. This work enables the IEA to recognize the driving pattern within a shorter time and therefore helps the IEA to have a better dynamic performance. The work has been submitted to the IEEE Transaction on Intelligent Transportation Systems.

 \end{enumerate}

\section{Outline of the dissertation}
This dissertation is organized into seven chapters. After introducing the research background and reviewing some related topics in Chapter 1 and Chapter 2, we firstly compared the three key topologies for the battery-ultracapacitor HESS in Chapter 3. Also in that chapter, a new approach using combined simulation tools of both Matlab and Python has been introduced, and the vehicle powertrain system model and the simulation platform have been established. In Chapter 4, the extended Support Vector Machine (e-SVM) with an embedded feature selection ability is introduced. In Chapter 5, the Inertial Matching Pursuit Classification (IMPC), is proposed especially for recognizing vehicle driving patterns within a shorter time period. Both Chapter 4 and 5 are devoted to the pattern recognition problem, with Chapter 4 focusing on the feature selection problem and Chapter 5 focusing on improving the dynamic property of the pattern recognition system. To achieve the goal of accurately recognized driving pattern, the Intelligent Energy Allocation (IEA) algorithm is finally presented in Chapter 6. In this approach, the IEA algorithm take driving data as input, to accurately recognize the Trip Mode in real time. With the recognized driving patterns, the IEA algorithm generates the optimal energy control actions to distribute the required current between the battery and ultracapacitor according to the learned Q network. In Chapter 7, we conclude the entire thesis and discuss the future work. The detailed structure of the thesis is given in the following list and Figure \ref{chapter_diag}

% ============== figure 1.3 ==============
\begin{figure}[hbt]
  \center
  \includegraphics[width=0.9\textwidth, height = 0.55\textwidth]{chapters/1/images/chapter_flow.eps}
  \caption{Outline of the dissertation}
  \label{chapter_diag}
\end{figure}
% ========================================

\begin{description}
\item[\textbf{Chapter 1}] Introduction and claims of this research
\item[\textbf{Chapter 2}] Review on the HESS design/control problems and related topics
\item[\textbf{Chapter 3}] Comparison of the HESS topologies, building of the object models, and introduction of combined simulation platform. Parts of this work was presented at the 2011 ASME International Mechanical Engineering Congress and Exposition. 
\item[\textbf{Chapter 4}] Presentation of the new extended SVM for driving pattern recognition and discussion on the feature selection problem of the driving pattern recognition system. An early version of the work was published on the Journal of the Franklin Institute in 2015.
\item[\textbf{Chapter 5}] Presentation of the Inertial Matching Pursuit Classification algorithm for driving pattern recognition and discussion on the sampling period problem. This work was submitted to the IEEE Transactions on Intelligent Transportation systems.
\item[\textbf{Chapter 6}] Introduction of the Intelligent Energy Allocation algorithm (IEA)
\item[\textbf{Chapter 7}] Summary of this research and discussion of future work.
\end{description}

