---
title: Responses to comments and Log of changes
author: Jason X Zhang
---

Received on 2017-11-07 | 26 comments in all

### -> Comment 1: Page 10
>?

Table of figures error. Fixed

### -> Comment 2: Page 19
>should connect back to need for ESS/HESS in each type of electrified vehicle architecture

Added some introduction about the need for ESS/HESS in each type of electrified vehicle architecture

### -> Comment 3: Page 20
>reference? be careful; can't include figures from other publications in a thesis; have to get permission from author, or re-draw somehow

I did add reference for this plot, but it is not in the caption of the plot. Please check the screen shot below:

<img src="comment3.png" alt="Ragone plot in the thesis" width="650" height="650"/>

<a name="c4"></a>

### -> Comment 4: Page 21
>not clear if this package from machine learning or HESS

This package is developed for HESS simulation in Python

[Source Code](https://bitbucket.org/JasonX_Zhang/simulator/src/174328217bcf?at=master)

[Pypi Homepage](https://pypi.python.org/pypi/simulator/1.0.3)

[Online App](http://18.220.213.47:8888/simulator/), Password: `uvic`

More explanation has been added to the thesis

### -> Comment 5: Page 21
>Need to tie together these various bits better; ie. connect how they incrementally develop the capability to optimally control an HESS

Added some explanation in this part

### -> Comment 6: Page 21
>what's the status of this one? Can you send the final draft?

I sent the final draft to you and Zuomin on Jan 11 2017. I am still waiting for some feedback and have not submitted.

I will find the draft and send it again. :)

### -> Comment 7: Page 22
>also again, instead of just a list, connect the dots for the reader to show how each piece is connected together

Add some explanation

### -> Comment 8: Page 22
>Seems quite a generic name for a specific package?

Please see [Comment 4](#c4). I am lucky that no one took this name :)

### -> Comment 9: Page 22
>presuming these chapters are built off the papers; state this explicitly here, and at the start of each chapter give as full a reference as possible to the journal/conference where the work was presented

Added

### -> Comment 10: Page 37
>need again some intro text to tie together the sections in this chapterw

Added some intro

### -> Comment 11: Page 39
>this isn't a type of topology; weird header for section

All of those three topologies are compared by using this Typical Pulsed Current Load. That's why this load is firstly introduced in this section.

### -> Comment 12: Page 39
>give an idea of time lengths; e.g is this PWM freq, or drivecycle power demands timescales

As mentioned in the manuscript and reference [A. Kuperman 2011], the majority of electric and hybrid vehicles load can be closely represented by a typical pulsed current load. So, $T$ here is the period or $1/freq$ of the typical pulsed current load. For example, if $T=0$, then we will have a direct-current $i_{L,MIN}u(t)$.

### -> Comment 13: Page 48
>how do Matlab/Python connect?

They are connected by json file. In fact, they are not executed at the same time. The vehicle model in Matlab is firstly run, and the current load data is generated and saved into a json file. Then python loads those data from the json file and executes the HESS model. Added some explanation and a diagram

### -> Comment 14: Page 50
>again, not clear how these pieces fit together, adn what 'Simulator' actually does; maybe a diagram to show the relationships?

Added a diagram

### -> Comment 15: Page 55
>what' the final integration of these sub-models? how are eqn's solved? in time? etc

Since we have a one order system and in order to avoid differential equation, I converted ODEs into integral and use Trapezoid Rule to approximating a definite integral.
Some explanation has been added in the thesis.
Here is an [example](https://bitbucket.org/JasonX_Zhang/simulator/src/174328217bcf2125eaf86e4b2d93b70261db1cb9/simulator/models/electrical/HessPassive.py?at=master&fileviewer=file-view-default) for converting ODE to integral in Simulator.

### -> Comment 16: Page 56
>tie title back to drivetrain features

Changed title to "Powertrain Feature Selection Based on Extended Support Vector Machine"

### -> Comment 17: Page 56
>section 1

Added Section 1

### -> Comment 18: Page 56
>why? what are you going to do once know driving condition?

Added some explanation here.

### -> Comment 19: Page 74
>so how does this relate to the previous chapter?
How are the algorithms and purposes of each different/the same?

Added some explanation on the purpose of this chapter

### -> Comment 20: Page 98
>different than just controlling a battery in isolation

In a battery-only energy storage system, we are not able to fully control the battery. All we can do is to monitor the battery status and to control its thermal performance by actively control the temperature of it, but we are not able to control its current or voltage. Thus, it is not possible to improve its efficiency. Only in a hybrid system, we have the freedom to change the battery current, by asking the UC to provide a part of the requested power.

### -> Comment 21: Page 98
>again, how does this build off knowing driving conditions from algorithms in previous chapters?

Explanation added.

### -> Comment 22: Page 101
>comment on usefulness of DP techniques to offline vs realtime applications

Please see the "Reinforcement Learning vs. Dynamic Programming" in 6.1.2

Added some explanation about the usefulness of DP

### -> Comment 23: Page 109
>tie into need for DP algorithms

Because of DP's assumption of a perfect model and its great computational expense, it is not used in the proposed Intelligent energy allocation algorithm. Instead, Reinforcement Learning is employed.


### -> Comment 24: Page 132
>this is better at tying things together; need to do similiar throughout the rest of the document

Yes, added similar explanation throughout the rest of the document.

### -> Comment 25: Page 132
>need to re-start key contributions, in addition to stating what was developed done, need to tease out what's unique and how good it is relative to existing litertaure/methods

re-started key contribution and added some explanation about the superiority of the proposed algorithms.

### -> Comment 26: Page 134
>what about trying on a real HESS? experimental validation? larger simulated datasets with ESS degredation models?

Yes, this makes sense to me. Added.
